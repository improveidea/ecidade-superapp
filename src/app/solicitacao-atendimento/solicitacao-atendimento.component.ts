import { CpfCnpjValidator } from '../validators/CpfCnpjValidator';
import { AtributoService } from 'src/app/services/atributo.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {STEPPER_GLOBAL_OPTIONS, StepperSelectionEvent} from '@angular/cdk/stepper';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatStepper, MatStepperNext } from '@angular/material/stepper';
import { BreakpointObserver } from '@angular/cdk/layout';
import { BehaviorSubject } from 'rxjs';
// import Swal from 'sweetalert2';
import { cpfInvalidoValidator as CpfInvalidoValidator } from '../directives/cpf-invalido.directive';
import { cnpjInvalidoValidator as CnpjInvalidoValidator } from '../directives/cnpj-invalido.directive';
import { AttendanceService } from '../atendimento/services/attendance.service';
import { StateService } from '../services/state.service';
import { FormularioJSONService } from '../services/formularioJSON.service';
import { RequisicoesCamposService } from '../campos/services/requisicoes-campos.service';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { ObjectValidator } from '../validators/ObjectValidator';
import {ToastController} from '@ionic/angular';
import {FormsDataService} from './services/forms-data.service';
import {ApiService} from '../services/api/api.service';
import { environment } from '../../environments/environment';
import {UserService} from "../services/user/user.service";


@Component({
    selector: 'app-solicitacao-atendimento',
    templateUrl: './solicitacao-atendimento.component.html',
    styleUrls: ['./solicitacao-atendimento.component.scss'],
    providers: [{
        provide: STEPPER_GLOBAL_OPTIONS, useValue: { showError: true }
    }]
})
export class SolicitacaoAtendimentoComponent implements OnInit {

    @ViewChild('stepper', {static: false}) stepper: MatStepper;
    @ViewChild(MatStepperNext, {static: false}) btnNextStep: MatStepperNext;

    public currentStep = 0;
    public displaySteps: number[] = [];
    public resutadoIndex = 1;
    public secoesFormulario;
    public form: FormGroup = new FormGroup({});
    public readonly = false;
    public title: string;
    public idTipo: number;
    public idDepartamento: number;
    public idFormaReclamacao: number;
    public dadosFormOld: any = {form: {}, tabela: {}};
    public user: any = {};
    public dados: any = {};
    public isInternalError = false;
    public arquivos: any[] = [];
    public codigoAtendimentoAnterior: any;
    public secoesTabela: Map<number, any> = new Map();
    public cleanedForm: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public nextStepClick: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public erroTabelaSecao: BehaviorSubject<any | null> = new BehaviorSubject<any>(null);
    public valoresSecaoTabela: BehaviorSubject<Map<string, any>> = new BehaviorSubject<Map<string, any>>(new Map());
    public showLoading = false;
    public acao = '';
    public buttonEnviarDisabled = false;
    public hideProgressBar = true;
    public loading = false;
    public icones = {faQuestionCircle};
    public linksaibamais;
    public evento: any = {};
    private formJSON: any;

    constructor(
        private readonly attendance: AttendanceService,
        private readonly state: StateService,
        private readonly api: FormsDataService,
        private readonly apiService: ApiService,
        private readonly formularioJSONService: FormularioJSONService,
        private readonly formBuilder: FormBuilder,
        private readonly breakPointObs: BreakpointObserver,
        private readonly route: Router,
        private readonly requisicoesCampos: RequisicoesCamposService,
        private routeActived: ActivatedRoute,
        private readonly atributoService: AtributoService,
        private toast: ToastController,
        private userService: UserService
    ) {
    }

    ngOnInit() {

        this.codigoAtendimentoAnterior = this.routeActived.snapshot.paramMap.get('codigoProcesso');
        if (this.codigoAtendimentoAnterior) {
            this.api.get(`solicitacoes/atendimento/${this.codigoAtendimentoAnterior}`).subscribe(response => {
                this.dadosFormOld = response;
        this.montarFormulario();
            });
        } else {
            this.montarFormulario();
        }


        this.atributoService.receiveShowLoading().subscribe((show) => {
            this.showLoading = show;
        });
    }

    async montarFormulario() {
       // const state = this.state.getValueFromSession();

        const userDocument =  this.userService.getDocument();
        const type = this.state.getValue('tipo');
        this.idTipo = type.id;
        this.idDepartamento = type.depto_id;
        this.idFormaReclamacao = type.formareclamacao;
        this.linksaibamais = type.linksaibamais;
        this.loading = true;
        this.formularioJSONService.getBody(type).subscribe(async (formJSON) => {
            try {
                const formObject = formJSON.formulario;

                this.formJSON = formObject;
                // @ts-ignore
                this.acao = formObject.acao || '';

                if (this.formJSON.hasOwnProperty('evento')) {
                    this.evento = this.formJSON.evento;
                }
// @ts-ignore
                if (formObject && formObject.secoes) {
// @ts-ignore
                    this.secoesFormulario = formObject.secoes;
                    this.resutadoIndex += this.secoesFormulario.length;

                    const groupSecoes: any = {};
                    this.secoesFormulario.forEach((secao: any, indSecao: any) => {
                        const groupCampos: any = {};

                        secao.campos.forEach((campo: any) => {
                            let formControl: any;
                            const validators = this.addValidatorField(campo);

                            this.addEvento(secao, campo);

                            if (validators.length > 0) {
                                formControl = new FormControl('', validators);
                            } else {
                                formControl = new FormControl('');
                            }

                            groupCampos[campo.nome] = formControl;
                            campo.flexSize = this.ajustarTamanhoCampo(campo);
                        });

                        groupSecoes[secao.nome] = new FormGroup(groupCampos);

                        if (secao.tipo === 'tabela') {
                            this.secoesTabela.set(indSecao, secao);
                        }
                    });

                    this.form = new FormGroup(groupSecoes);
                }

                if (this.acao === 'atualizacao_cadastral' && !this.codigoAtendimentoAnterior) {

                    await this.verificaServidorPossuiPermissaoRecadastramento(
                        userDocument,
                        this.idTipo
                    );

                    setTimeout(() => {
                       this.form.get('dados_pessoais').get('cpf_servidor').patchValue(userDocument);
                        const element = document.getElementById('cpf_servidor');
                        //document.getElementById('cpf_servidor').value = ;
                    document.getElementById('cpf_servidor').focus();
                    document.getElementById('cpf_servidor').blur();
                    document.getElementById('cpf_servidor').dispatchEvent(new Event('blur'));
                    }, 1000);
                }

                if (this.codigoAtendimentoAnterior) {
                    this.api.get(`solicitacoes/atendimento/${this.codigoAtendimentoAnterior}`).subscribe(response => {
                        this.preencherCamposRefazer(response);
                    });
                }

                this.loading = false;
            } catch (e) {
                this.route.navigate(['/error']);
                this.loading = false;
                const msg = 'Ops... algo inesperado aconteceu!. Já estamos verificando. Tente novamente mais tarde.';
                this.presentToast(msg, 3000);
            }
        },  (error) => {
            this.isInternalError = true;
            const msg = 'Ops... algo inesperado aconteceu!. Já estamos verificando. Tente novamente mais tarde.';
            this.presentToast(msg, 3000);
        });

       // this.title = state.tipo.descricao;
        this.state.load();

        this.attendance.get().subscribe(attendance => {
            this.dados = attendance.dados;
            this.arquivos = attendance.arquivos;
        });
    }
    private addValidatorField(campo: any): any[] {

        const validators = [];

        if (campo.tipo === 'cpf') {
            validators.push(CpfInvalidoValidator());
        }

        if (campo.tipo === 'cnpj') {
            validators.push(CnpjInvalidoValidator());
        }

        if (campo.obrigatorio) {
            validators.push(Validators.required);
        }

        if (campo.tipo === 'email') {
            validators.push(Validators.email);
        }

        if (campo.tipo === 'autocomplete') {
            validators.push(ObjectValidator);
        }

        if (campo.tipo === 'cpf_cnpj' || campo.tipo === 'cpfCnpj') {
            validators.push(CpfCnpjValidator);
        }

        return validators;
    }

    private async preencherCamposRefazer(response) {
        if (response && response.secoes) {
            const secoes = response.secoes;

            await secoes.forEach((secao: any) => {
                if (secao.campos && !secao.resposta && secao.nome !== 'anexo') {
                    secao.campos.forEach((campo: any) => {
                        if (this.form.get(secao.nome) && this.form.get(secao.nome).get(campo.nome)) {
                            const field = this.form.get(secao.nome).get(campo.nome);

                            if (campo.tipo === 'lista' || campo.tipo === 'lista_dinamica') {
                                field.patchValue(campo.resposta.codigo || campo.resposta.id, {emitEvent: true});
                            } else {
                                field.setValue(campo.resposta);
                            }
                        }
                    });
                } else {
                    if (secao.tipo === 'tabela' && secao.resposta) {
                        this.preecherCamposRefazerTabela(secao);
                    }

                    if (secao.tipo === 'anexo') {
                        this.attendance.setValue('arquivos', secao.resposta);
                        this.attendance.persist();
                        setTimeout(() => {
                            if (secao.resposta instanceof Array) {
                                secao.resposta.forEach(el => {
                                    const domEvent = new CustomEvent('OnAddItem', {detail: el});
                                    const element = document.getElementById(`${secao.nome}-${el.nome}`);

                                    if (element) {
                                        element.dispatchEvent(domEvent);
                                    }
                                });
                            }
                        }, 1000);

                    }
                }
            });

            setTimeout(() => {
                for (const index in this.evento) {
                    if (index === 'observar') {
                        this.evento[index].forEach((object: any) => {
                            object.campos.forEach((evento: any) => {
                                this.observarEvento(evento.nome, evento.secao, object);
                            });
                        });
                    }
                }
            }, (1000));
        }
    }

    private preecherCamposRefazerTabela(secao) {
        if (secao.resposta instanceof Array) {
            secao.resposta.forEach((element) => {
                for (const el of Object.entries(element)) {
                    if (typeof element[el[0]] === 'object') {
                        // @ts-ignore
                        element[el[0]] = (el[1].codigo || el[1].id);
                    }
                }
                const domEvent = new CustomEvent('OnAddItem', {detail: element});
                document.getElementById('secao_' + secao.nome).dispatchEvent(domEvent);
            });
        }
    }

    private verificaServidorPossuiPermissaoRecadastramento(cpf, tipoProcesso) {
        this.api.get(
            `v4/api/patrimonial/ouvidoria/atendimento/processo-eletronico/servidor/${cpf}/tipoprocesso/${tipoProcesso}&type=ecidade`
        )
            .toPromise()
            .then(resp => {
                if (resp.data.pertence !== true) {
                    this.presentToast('Você não pertence a esta secretaria' , 3000);
                    this.route.navigate(
                        ['/home'],
                        { queryParams: { message: 'Você não pertence a esta secretaria' } }
                    );
                }

            })
            .catch(() => {
                this.presentToast('Você não pertence a esta secretaria' , 3000);
                this.route.navigate(
                    ['/home'],
                    { queryParams: { message: 'Você não pertence a esta secretaria' } }
                );
            });
    }

    private addEvento(secao: any, campo: any) {
        if (Object.keys(this.evento).length > 0) {
            for (const index in this.evento) {
                if (index !== undefined) {
                    switch (index) {
                        case 'observar':
                            for (const indexX in this.evento[index]) {
                                if (indexX !== undefined) {
                                    this.observar(this.evento[index][indexX], secao, campo);
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    private observar(observar: any, secao: any, campo: any) {
        if (
            observar.hasOwnProperty('data') &&
            observar.data.hasOwnProperty('campos')
        ) {
            const campos = observar.campos;

            for (const index in campos) {
                if (campos[index].nome === campo.nome) {
                    const evento: any = observar;

                    evento.nome = 'observarEvento';

                    if (campo.hasOwnProperty('on')) {
                        campo.on.push(evento);
                    } else {
                        campo.on = [evento];
                    }
                }
            }
        }
    }

    private observarEvento(campo: any, secao: any, evento: any) {
        let event: boolean = false;
        const object = {
            campoEvento: null,
            value: null
        };

        for (const index in evento.campos) {
            if (typeof evento.campos[index] === 'object') {
                const campoEvento = evento.campos[index];

                if (this.form.get(campoEvento.secao) && this.form.get(campoEvento.secao).get(campoEvento.nome)) {
                    const campoForm = this.form.get(campoEvento.secao).get(campoEvento.nome);

                    if (campoEvento.hasOwnProperty('valor')) {
                        let ingorarValores: any = [];
                        const valor = campoEvento.valor;

                        if (evento.data.hasOwnProperty('ingorar_valores')) {
                            ingorarValores = evento.data.ingorar_valores;
                        }

                        if (
                            campoForm.value != null &&
                            ingorarValores.indexOf(campoForm.value) >= 0
                        ) {
                            event = false;
                        } else if (
                            valor === 'request_all_text' &&
                            campoForm.value !== null &&
                            campoForm.value.length > 0 &&
                            campoForm.value.trim() !== ''
                        ) {
                            object.campoEvento = campoEvento;
                            object.value = campoForm.value;
                            event = true;

                            break;
                        } else if (valor !== 'request_all_text') {
                            if (valor.indexOf(campoForm.value) >= 0) {
                                object.campoEvento = campoEvento;
                                object.value = campoForm.value;
                                event = true;

                                break;
                            }
                        }
                    }
                }
            }
        }

        if (event) {
            this.requerir_se(evento.data.campos);

            return;
        }

        this.nao_requerir(evento.data.campos);
    }

    private nao_requerir(campos: any) {
        for (const index in campos) {
            if (this.form.get(campos[index].secao) && this.form.get(campos[index].secao).get(campos[index].nome)) {
                const campoForm = this.form.get(campos[index].secao).get(campos[index].nome);

                campoForm.clearValidators();
                campoForm.reset();

                if (campos.secao === 'anexo') {
                    this.resetValidatorsSection('anexo');
                }

            }
        }
    }

    private requerir_se(campos: any) {
        for (const index in campos) {
            if (campos[index] !== undefined) {
                const secao = this.form.get(campos[index].secao);
                if (secao && secao.get(campos[index].nome)) {
                    const campoForm = secao.get(campos[index].nome);

                    campoForm.setValidators([
                        Validators.required
                    ]);

                    if (
                        campoForm.value == null ||
                        campoForm.value.length === 0
                    ) {
                        campoForm.setValue(null);
                    }

                    if (campos.secao === 'anexo') {
                        this.resetValidatorsSection('anexo');
                    }

                    campoForm.updateValueAndValidity();
                }
            }
        }
    }

    public customChange(event: any) {
        if (
            event.hasOwnProperty('eventos') &&
            event.hasOwnProperty('campo') &&
            event.hasOwnProperty('secao')
        ) {
            for (const index in event.eventos) {
                if (event.eventos[index].hasOwnProperty('nome')) {
                    const nome = event.eventos[index].nome;

                    switch (nome) {
                        case 'observarEvento':
                            this.observarEvento(
                                event.campo,
                                event.secao,
                                event.eventos[index]
                            );
                            break;
                    }
                }
            }
        }
    }

    private ajustarTamanhoCampo(campo: any): string {

        let tamanho = campo.tamanho;

        if (Number(tamanho) === 100) {
            return '100%';
        }

        const tamanhoCampo = campo.tamanhoCampo;

        if (campo.tamanhoCampo && Number(tamanhoCampo) === 100) {
            return '100%';
        } else {
            if (campo.tamanhoCampo) {
                tamanho = tamanhoCampo;
            }
        }

        return `${tamanho}% - 11px`;
    }

    private clearValidatorsSection(section): void {
        this.secoesFormulario.forEach(secao => {

            if (secao.nome !== section.nome) {
                return;
            }

            secao.campos.forEach(campo => {
                this.form.get(secao.nome).get(campo.nome).clearValidators();
                this.form.get(secao.nome).get(campo.nome).updateValueAndValidity();
            });
        });
    }

    private resetValidatorsSection(section): void {
        this.secoesFormulario.forEach(secao => {
            if (section === undefined) {
                return;
            }
            if (secao.nome !== section.nome) {
                return;
            }

            secao.campos.forEach(campo => {

                const validators = this.addValidatorField(campo);

                this.form.get(secao.nome).get(campo.nome).setValidators(validators);
                this.form.get(secao.nome).get(campo.nome).updateValueAndValidity();
                this.form.get(secao.nome).get(campo.nome).reset();
                this.form.get(secao.nome).get(campo.nome).markAsTouched();
            });
        });
    }

    private validarSecaoComTabela(secao: any): boolean {

        let valid = true;
        let mensagem = null;
        const mapValoresSecaoTabela = this.valoresSecaoTabela.getValue();
        const mapValoresSecao = mapValoresSecaoTabela.get(secao.nome);

        if (secao.validacoes && secao.validacoes.length > 0) {
            secao.validacoes.forEach(validacao => {
                switch (validacao.tipo) {
                    case 'quantidade_campo':
                        let valoresEncontrados = 0;
                        if (mapValoresSecao.size >= 1) {
                            validacao.campos.forEach(validacaoCampo => {
                                mapValoresSecao.forEach(linha => {
                                    const valorInformado = linha[validacaoCampo.nome];
                                    valoresEncontrados += validacaoCampo.valor.find(valor => valor === valorInformado) ? 1 : 0;
                                });
                            });

                            if (valoresEncontrados < validacao.minimo) {
                                valid = false;
                            }
                        }
                        break;

                    case 'quantidade':
                        if (mapValoresSecao.size < validacao.minimo) {
                            valid = false;
                        }
                        break;
                }

                if (valid === false && mensagem === null) {
                    mensagem = validacao.mensagem;
                }
            });
        }

        if (!valid) {
            this.resetValidatorsSection(secao);
            this.erroTabelaSecao.next({
                secao: secao.nome,
                erro: mensagem
            });
            return false;
        }

        this.erroTabelaSecao.next(null);
        this.clearValidatorsSection(secao);
        return true;
    }

    gerenciarSecaoComTabela() {

        const secaoAtual = this.secoesTabela.get(this.currentStep);

        /**
         * Se a secao atual for uma secao de tabela
         * reseta os validadores do formulario
         */
        if (secaoAtual !== null || secaoAtual !== undefined) {
            this.resetValidatorsSection(secaoAtual);
        }
    }

    async obterDados(): Promise<any> {

        return new Promise((res) => {

            const mapValoresSecaoTabela = this.valoresSecaoTabela.getValue();
            const dados = { ...this.form.value };

            this.secoesFormulario.forEach(secao => {

                if (secao.tipo === 'tabela') {

                    const valoresSecao = mapValoresSecaoTabela.get(secao.nome);
                    const valores = [];

                    valoresSecao.forEach(valorSecao => {

                        const valor = { ...valorSecao };

                        secao.campos.forEach(campo => {

                            switch (campo.tipo) {

                                case 'lista':
                                    // eslint-disable-next-line max-len
                                    const opcaoLista = campo.opcoes.find(opcao => (opcao.id === valor[campo.nome] || opcao.codigo === valor[campo.nome]));
                                    if (opcaoLista) {
                                        valor[campo.nome] = opcaoLista;
                                    }
                                    break;

                                case 'lista_dinamica':

                                    const requisicoesCampos = this.requisicoesCampos;

                                    if (!requisicoesCampos) {
                                        break;
                                    }

                                    const fieldOptions = requisicoesCampos.getValue(campo.nome, 'opcoes');
                                    let valorCampoListaDinamica = valor[campo.nome];

                                    if (fieldOptions instanceof Array) {

                                        fieldOptions.forEach(opcao => {
                                            if (opcao) {
                                                if (opcao.id && opcao.id === valorCampoListaDinamica) {
                                                    valorCampoListaDinamica = opcao;
                                                } else if (opcao.codigo && opcao.codigo === valorCampoListaDinamica) {
                                                    valorCampoListaDinamica = opcao;
                                                }
                                            }
                                        });
                                    }

                                    valor[campo.nome] = valorCampoListaDinamica;
                                    break;

                                case 'date':
                                case 'data':
                                    // eslint-disable-next-line max-len
                                    if (valor[campo.nome] && typeof valor[campo.nome] === 'object' && !(valor[campo.nome] instanceof Date)) {
                                        valor[campo.nome].format('L');
                                    }
                                    break;
                                case 'cpfCnpj':
                                case 'cep':
                                    if (valor[campo.nome]) {
                                        valor[campo.nome] = valor[campo.nome].replace(/\D/g, '');
                                    }
                                    break;
                                case 'button-dialog':
                                    if (!this.requisicoesCampos) {
                                        break;
                                    }

                                    valor[campo.nome] = this.requisicoesCampos.getValue(campo.nome, 'opcoes');
                                    break;
                            }
                        });

                        valores.push(valor);
                    });

                    dados[secao.nome] = valores;

                } else {

                    secao.campos.forEach(campo => {

                        let valor = this.form.get(secao.nome).get(campo.nome).value;

                        switch (campo.tipo) {
                            case 'date':
                            case 'data':
                                if (valor && typeof valor === 'object' && !(valor instanceof Date)) {
                                    valor = valor.format('L');
                                }
                                break;

                            case 'lista':
                                const opcaoLista = campo.opcoes.find(opcao => (opcao.id === valor || opcao.codigo === valor));
                                if (opcaoLista) {
                                    valor = opcaoLista;
                                }
                                break;

                            case 'lista_dinamica':

                                const requisicoesCampos = this.requisicoesCampos;

                                if (!requisicoesCampos) {
                                    break;
                                }

                                const fieldOptions = requisicoesCampos.getValue(campo.nome, 'opcoes');

                                if (fieldOptions instanceof Array) {

                                    fieldOptions.forEach(opcao => {
                                        if (opcao) {
                                            if (opcao.id && opcao.id === valor) {
                                                valor = opcao;
                                            } else if (opcao.codigo && opcao.codigo === valor) {
                                                valor = opcao;
                                            }
                                        }
                                    });
                                }
                                break;
                            case 'dialog':
                                const onClose = campo.opcoes.onClose;
                                dados[secao.nome][onClose.nome] = this.attendance.getValue(onClose.nome);

                                const dadosSession = sessionStorage.getItem(campo.nome);
                                const dadosCampoSession = JSON.parse(dadosSession);

                                if (dadosCampoSession) {
                                    valor = dadosCampoSession.find(item => ((item.codigo || item.id) === valor));
                                }
                                break;
                            case 'cpfCnpj':
                            case 'cep':
                                if (valor) {
                                    valor = valor.replace(/\D/g, '');
                                }
                                break;
                            case 'button-dialog':
                                if (!this.requisicoesCampos) {
                                    break;
                                }

                                valor = this.requisicoesCampos.getValue(campo.nome, 'opcoes');
                                break;
                        }

                        dados[secao.nome][campo.nome] = valor;
                    });
                }
            });

            res(dados);
        }).catch((error) => {
            console.log(error);
        });
    }

    getFormGroupControl(controlName: string): any {
        return this.form.get(controlName);
    }

    clear(Event: Event) {
        Event.preventDefault();
        this.clearForm();
    }

    clearForm() {
        this.cleanedForm.next(true);
        this.currentStep = 0;
        this.form.reset();
        this.stepper.reset();
        this.attendance.setValue('dados', {});
        this.attendance.setValue('arquivos', []);
        this.attendance.persist();
        this.atributoService.sendDialogClear();

        if (this.formJSON.hasOwnProperty('onCleaning')) {
            if (this.formJSON.onCleaning.hasOwnProperty('block')) {
                this.blockOnCleaning();
            }
        }
    }

    blockOnCleaning() {
        const formJSON = this.formJSON;
        const block = formJSON.onCleaning.block;

        if (block.hasOwnProperty('reference')) {
            const reference = block.reference;
            const section = formJSON.secoes.find(secao => secao.nome === reference.section);
            const field = section.campos.find(fieldFind => fieldFind.nome === reference.field);

            if (field.hasOwnProperty('change')) {
                if (field.change.hasOwnProperty('campos_preencher')) {
                    const camposPreencher = field.change.campos_preencher;

                    camposPreencher.forEach(fieldAux => {
                        if (fieldAux.hasOwnProperty('desbloquear') && fieldAux.desbloquear) {
                            this.form.get(field.secao || reference.section).get(field.campo).disable();
                        }
                    });
                }
            }
        }
    }

    display(index: number) {
        // return this.currentStep === index || this.displaySteps.includes(index);
    }

    getValoresComponenteTabela(campoTabelaValores) {

        const mapValoresSecaoTabela = this.valoresSecaoTabela.getValue();
        mapValoresSecaoTabela.set(campoTabelaValores.secao, campoTabelaValores.valores);

        this.valoresSecaoTabela.next(mapValoresSecaoTabela);
    }

    onChangeStep(event: StepperSelectionEvent) {

        if (!this.displaySteps.includes(event.selectedIndex)) {
            this.displaySteps.push(event.selectedIndex);
        }
        this.currentStep = event.selectedIndex;

        this.gerenciarSecaoComTabela();

        // this.obterDados().then(dados => {

        //   this.attendance.setValue('dados', dados);
        //   this.attendance.setValue('step', this.currentStep);
        //   this.attendance.persist();
        // })
    }

    previousStep(ev) {
        this.stepper.previous();
    }

    nextStep(ev) {

        const secaoAtualComTabela = this.secoesTabela.get(this.currentStep);

        this.nextStepClick.next(true);

        if (secaoAtualComTabela === null || secaoAtualComTabela === undefined) {
            return this.next();
        }

        if (!this.validarSecaoComTabela(secaoAtualComTabela)) {
            return;
        }
        this.next();
    }

    next() {
        this.stepper.next();
    }

    async send(event: Event) {

        await this.obterDados().then(dados => {
            this.attendance.setValue('dados', dados);
            this.attendance.setValue('step', this.currentStep);
            this.attendance.persist();
            this.dados = dados;
        });

        this.buttonEnviarDisabled = true;
        this.nextStepClick.next(true);

        event.preventDefault();

        const formularioRespondidoJSON = {
            // eslint-disable-next-line @typescript-eslint/naming-convention
            tipo_processo: this.idTipo,
            descricao: this.title,
            acao: this.acao,
            secoes: []
        };

        const respostasAnexo = [];

        this.secoesFormulario.forEach(s => {

            const secao = { ...s };
            secao.campos = [];

            s.campos.forEach(c => {

                const campo = { ...c };

                campo.resposta = this.dados[s.nome][c.nome] || '';

                if (c.tipo === 'dialog') {
                    secao.campos.push({
                        nome: c.opcoes.onClose.nome,
                        tipo: c.tipo,
                        resposta: this.dados[s.nome][c.opcoes.onClose.nome]
                    });
                }

                if (s.tipo.indexOf('anexo') > -1) {

                    this.arquivos.forEach(arquivo => {

                        respostasAnexo.push({
                            nome: arquivo.nome,
                            descricao: arquivo.descricao,
                            codigo: null,
                        });
                    });
                }

                secao.campos.push(campo);
            });

            if (s.tipo.indexOf('anexo') > -1) {
                secao.resposta = respostasAnexo;
            }

            if (s.tipo.indexOf('tabela') > -1) {
                secao.resposta = this.dados[s.nome];
            }

            formularioRespondidoJSON.secoes.push(secao);
        });

        if (this.form.status === 'INVALID') {

            this.secoesFormulario.forEach(secao => {

                if (this.form.get(secao.nome).status === 'INVALID') {
                    secao.campos.forEach(campo => {
                        if (this.form.get(secao.nome).get(campo.nome).status === 'INVALID') {
                            this.form.get(secao.nome).get(campo.nome).markAsTouched();
                        }
                    });
                }
            });
            this.buttonEnviarDisabled = false;
            return;
        }

        this.sendForm(formularioRespondidoJSON);
    }

    sendForm(formularioRespondido: any) {

        this.hideProgressBar = false;

        return new Promise(function(resolve, reject) {

            const envioArquivos = [];
            let existeAnexo = false;

            this.arquivos.forEach(arquivo => {

                existeAnexo = true;

                envioArquivos.push(new Promise(function(res, rej) {

                    if (arquivo.codigo) {
                        res(arquivo);
                        return;
                    }
                    const formData = new FormData();
                    formData.append('documento', this.base642Blob(arquivo.conteudo, arquivo.type), arquivo.descricao);

                    this.api.post('uploads', formData, true).subscribe(response => {
                        const anexo = {
                            codigo: null,
                            descricao: arquivo.descricao,
                            nome: arquivo.nome
                        };

                        anexo.codigo = response.id;
                        res(anexo);

                    }, error => {
                        rej(error);
                    });
                }.bind(this)));
            });

            if (!existeAnexo) {
                resolve(formularioRespondido);
            }

            Promise.all(envioArquivos)
                .then(anexos => {

                    formularioRespondido.secoes.forEach(secao => {
                        if (secao.tipo === 'anexo') {
                            secao.resposta = anexos;
                        }
                    });

                    resolve(formularioRespondido);
                })
                .catch(error => {
                    this.hideProgressBar = true;
                    reject(error);
                });

        }.bind(this))
            .then(async (formulario: any) => {

                /**
                 * VALIDA SE USUARIO EXISTE NO EAUTH
                 */
                if (Number(this.idFormaReclamacao) === 9) {

                    let cpfcnpj = null;
                    let email = null;
                    if (formulario.secoes) {
                        formulario.secoes.forEach(secao => {
                            if (secao.nome === 'requerente') {
                                secao.campos.forEach(campo => {

                                    if (campo.nome === 'cpf') {
                                        cpfcnpj = campo.resposta;
                                    }

                                    if (campo.nome === 'cnpj') {
                                        cpfcnpj = campo.resposta;
                                    }

                                    if (campo.nome === 'cpfcnpj') {
                                        cpfcnpj = campo.resposta;
                                    }

                                    if (campo.nome === 'email') {
                                        email = campo.resposta;
                                    }

                                });

                                }
                            });
                        }
                }

                const type = this.state.getValue('tipo');
                if (!formulario.descricao || formulario.descricao === '') {
                  formulario.descricao = type.nome
                }
                const dados = {
                    dados: JSON.stringify(formulario),
                    descricao: type.nome,
                    user: this.user,
                    tipo_processo: this.idTipo,
                    departamento: this.idDepartamento,
                    formareclamacao: this.idFormaReclamacao,
                    codigoAtendimentoAnterior: this.codigoAtendimentoAnterior,
                };

                this.apiService.post('forms', dados)
                    .subscribe(response => {
                        this.clearForm();
                        let msg = 'A sua solicitação foi enviada!';
                        msg += '\nEsse é o seu número de atendimento.';
                        msg += ' Utilize-o para acompanhar sua solicitação.';

                        if (response.status) {
                            msg = response.status;
                        }

                        const numeroAtendimento = response.atendimento.numero + '/' + response.atendimento.ano;
                        this.buttonEnviarDisabled = false;
                        this.hideProgressBar = true;
                        msg = `${msg}\n${numeroAtendimento}`;
                        this.presentToast(msg, 10000);

                        this.route.navigate(['/home']);
                    }, error => {
                        this.hideProgressBar = true;
                        this.buttonEnviarDisabled = false;
                        this.presentToast('Não foi possível enviar sua solicitação, tente mais tarde', 3000);
                    });
            })
            .catch(e => {
                this.hideProgressBar = true;
                this.buttonEnviarDisabled = false;
                console.error(e);
                //Swal.fire('Erro:', e.message, 'error');
            });
    }

    base642Blob(base64Data: string, contentType = 'text/plain', sliceSize = 512): Blob {

        const byteCharacters = atob(base64Data.split(',').pop());
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        return new Blob(byteArrays, { type: contentType });
    }

    get smallScreen() {
        return this.breakPointObs.isMatched('(max-width: 650px)');
    }

    async presentToast(msg, duration) {
        const toast = await this.toast.create({
            message: msg,
            duration: duration || 2000
        });
        toast.present();
    }
}
