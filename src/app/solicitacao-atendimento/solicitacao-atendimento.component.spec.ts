import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitacaoAtendimentoComponent } from './solicitacao-atendimento.component';

describe('SolicitacaoAtendimentoComponent', () => {
  let component: SolicitacaoAtendimentoComponent;
  let fixture: ComponentFixture<SolicitacaoAtendimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitacaoAtendimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitacaoAtendimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
