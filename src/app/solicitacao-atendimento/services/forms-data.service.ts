import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {ApiService} from '../../services/api/api.service';

@Injectable({
    providedIn: 'root'
})
export class FormsDataService {

    constructor(
        private readonly apiService: ApiService,
    ){}

    public get(path: string): Observable<any> {
        const endpoint = 'forms/data?url='+path;
        return this.apiService.get(endpoint);
    }
    public post(endpoint: string, data: any, formData = false): Observable<any> {

        const url = 'forms/data?url='+endpoint;
        return this.apiService.post(`${url}`, data, formData);
    }

}
