import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {PageHeaderModule} from '../components/page-header/page-header.module';
import {SolicitacaoAtendimentoComponent} from './solicitacao-atendimento.component';
import {MaterialModule} from '../core/material-module/material.module';
import {CamposModule} from '../campos/campos.module';
import {SecoesModule} from '../secoes/secoes.module';
import {ErrorModule} from '../components/error/error.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PageHeaderModule,
        ReactiveFormsModule,
        MaterialModule,
        CamposModule,
        SecoesModule,
        ErrorModule

    ],
    declarations: [SolicitacaoAtendimentoComponent],
    exports:[SolicitacaoAtendimentoComponent]

})
export class SolicitacaoAtendimentoModule {}
