import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirstAccessCityPage } from './first-access-city.page';

const routes: Routes = [
  {
    path: ':cityId',
    component: FirstAccessCityPage
  },
  {
    path: ':cityId/form',
    loadChildren: () => import('./access-form/access-form.module').then( m => m.AccessFormPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FirstAccessCityPageRoutingModule {}
