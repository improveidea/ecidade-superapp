import {Component, OnInit} from '@angular/core';
import {CityListService} from '../city-list/city-list.service';
import {ActivatedRoute, Router} from '@angular/router';
import {City} from '../models/City';
import {UserService} from '../services/user/user.service';
import {FormsDataService} from '../solicitacao-atendimento/services/forms-data.service';
import {StateService} from '../services/state.service';

@Component({
    selector: 'app-first-access-city',
    templateUrl: './first-access-city.page.html',
    styleUrls: ['./first-access-city.page.scss'],
})
export class FirstAccessCityPage implements OnInit {

    city = new City();
    currentCity = new City();
    firstAccessTypes = [];

    constructor(private cityService: CityListService,
                private activeRoute: ActivatedRoute,
                private userService: UserService,
                private router: Router,
                private state: StateService,
                private formDataService: FormsDataService
    ) {
    }

    async ngOnInit() {
        const cityId = this.activeRoute.snapshot.params.cityId;
        this.city = await this.cityService.getCitiesById(cityId);
        this.currentCity = await this.userService.setCurrentCity(this.city);
        await this.getFirstAccessTypes();
    }

    openForm(type) {
        this.state.setValue('tipo', type);
        this.router.navigate([`/first-access-city/${this.city.id}/form`]);
    }

    async getFirstAccessTypes() {
        const firstAccessTypes = await this.formDataService.get('solicitacoes/menu?formareclamacao=9').toPromise();
        for (const firstAccessType of firstAccessTypes) {
            for (const secretary of firstAccessType.children) {
                for (const processType of secretary.children) {
                    const type = {
                        institution: {
                            id: firstAccessType.codigo,
                            name: firstAccessType.nome,
                        },
                        secretary: {
                            id: secretary.codigo,
                            name: secretary.codigo,
                        },
                        process: {
                            department: processType.depto_id,
                            name: processType.nome,
                            help: processType.linksaibamais,
                            id: processType.id,
                            group: processType.formareclamacao
                        },
                        id: processType.id,
                        // eslint-disable-next-line @typescript-eslint/naming-convention
                        depto_id: processType.depto_id
                    };
                    this.firstAccessTypes.push(type);
                }
            }
        }
    }

}
