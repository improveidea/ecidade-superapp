import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {FirstAccessCityPageRoutingModule} from './first-access-city-routing.module';

import {FirstAccessCityPage} from './first-access-city.page';
import {PageHeaderModule} from '../components/page-header/page-header.module';
import {MaterialModule} from '../core/material-module/material.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FirstAccessCityPageRoutingModule,
        PageHeaderModule,
        MaterialModule
    ],
    declarations: [FirstAccessCityPage],

})
export class FirstAccessCityPageModule {
}
