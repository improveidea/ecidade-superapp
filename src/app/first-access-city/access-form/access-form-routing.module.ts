import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccessFormPage } from './access-form.page';

const routes: Routes = [
  {
    path: '',
    component: AccessFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccessFormPageRoutingModule {}
