import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {AccessFormPageRoutingModule} from './access-form-routing.module';

import {AccessFormPage} from './access-form.page';
import {PageHeaderModule} from '../../components/page-header/page-header.module';
import {SolicitacaoAtendimentoModule} from '../../solicitacao-atendimento/solicitacao-atendimento.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AccessFormPageRoutingModule,
        PageHeaderModule,
        SolicitacaoAtendimentoModule
    ],
    declarations: [AccessFormPage]
})
export class AccessFormPageModule {
}
