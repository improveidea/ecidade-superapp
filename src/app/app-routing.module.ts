import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'first-access',
    loadChildren: () => import('./first-access/first-access.module').then( m => m.FirstAccessPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule),

  },
  {
    path: 'city-list',
    loadChildren: () => import('./city-list/city-list.module').then( m => m.CityListPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'first-access-city',
    loadChildren: () => import('./first-access-city/first-access-city.module').then( m => m.FirstAccessCityPageModule)
  },
  {
    path: 'me-profile',
    loadChildren: () => import('./users/profile/profile.module').then( m => m.ProfilePageModule),
    canLoad: [AuthGuard]
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
