import { FormControl } from '@angular/forms';

export function ObjectValidator(c: FormControl) {
  if (typeof c.value !== 'object') {
    return {
      object : {
       valid: false
      }
    };
  }

  return null;
}
