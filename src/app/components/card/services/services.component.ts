import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
})
export class ServicesComponent implements OnInit {

  @Input() routeTo: string;
  @Input() icon: string;
  @Input() title: string;
  @Input() total: string;
  @Input() help: string;
  @Input() firstNumber: string;
  @Input() secondNumber: string;

  constructor() { }

  ngOnInit() {}

}
