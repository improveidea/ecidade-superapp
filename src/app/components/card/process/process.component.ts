import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ToastController} from "@ionic/angular";

@Component({
  selector: 'app-card-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.scss'],
})
export class ProcessComponent implements OnInit {

  @Input() processData: any;
  title: string;
  date: string;
  code: string;
  year: string;
  description: string;
  status: string;
  id: string;
  type: number;
  process_number: string;

  constructor(private _router: Router, private _toastController: ToastController,) { }

  ngOnInit() {
    this.title = this.processData.title;
    this.date = this.processData.date;
    this.code = this.processData.code;
    this.process_number = this.processData.process_number;
    this.year = this.processData.year;
    this.type = this.processData.type;
    this.status = this.processData.status;
    this.id = this.processData.id;
  }

  async showInfo(processData) {

    if (!this.id || this.id === '') {
      const toast = await this._toastController.create({
        message: 'Código do processo não encontrado.',
        duration: 1000,
      });
      toast.present();
      return;
    }
    localStorage.setItem('process', JSON.stringify(this.processData));
    this._router.navigateByUrl(`/home/process/info/${this.id}`);
  }

}
