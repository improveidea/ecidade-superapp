import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-logged-in',
  templateUrl: './logged-in.component.html',
  styleUrls: ['./logged-in.component.scss'],
})
export class LoggedInComponent implements OnInit {

  public cityName = '...';
  public userName = '...';

  constructor() { }

  async ngOnInit() {
    const data = JSON.parse(await localStorage.getItem('user_data'));
    this.userName = data.name;
    this.cityName = data.current_city.name;
  }

}
