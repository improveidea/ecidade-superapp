import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-no-information',
  templateUrl: './no-information.component.html',
  styleUrls: ['./no-information.component.scss'],
})
export class NoInformationComponent implements OnInit {

  @Input() show = true;
  @Input() description = 'Sem informações para exibir.';
  constructor() { }

  ngOnInit() { }

}
