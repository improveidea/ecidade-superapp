import {Component, NgZone, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../services/api/api.service';
import {InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser/ngx';
import {AuthService} from '../services/auth/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UserService} from '../services/user/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    providers;
    code = null;
    logginIn = false;
    constructor(
        private router: Router,
        private apiService: ApiService,
        private inAppBrowser: InAppBrowser,
        private authService: AuthService,
        private snackBar: MatSnackBar,
        private route: ActivatedRoute,
        private userService: UserService,
        private Zone: NgZone
    ) {
    }

    async ngOnInit() {
        console.log('chamou provider')
        const response = await this.apiService.get('authorization-providers').toPromise();

        if (response.data) {
            this.providers = response.data;
        }
        const loginCode = this.route.snapshot.queryParamMap.get('code');
        if (loginCode !== null && !this.authService.isLoggedIn()) {
            this.logginIn = true;
            this.exchangeCode(loginCode, true);
            this.logginIn = false;
        }
        if (this.authService.isLoggedIn()) {
            this.userService.getUserData();
            await this.router.navigateByUrl('/home');
        }
    }

    loginIn(provider) {
        const options: InAppBrowserOptions = {
            toolbarposition: 'top',
            location: 'no', // Or 'no'
            clearcache: 'yes',
            clearsessioncache: 'yes',
            hideurlbar: 'yes',
            zoom: 'no', // Android only ,shows browser zoom controls
            toolbar: 'yes', // iOS only
        };
        localStorage.setItem('current_provider', JSON.stringify(provider));
        const route = `${provider.redirect_url}&client_id=${provider.client_id}`;
        const iapReturn = this.inAppBrowser.create(route, '_blank', options);
        iapReturn.on('loadstart').subscribe(res => {
          this.logginIn = true;
            if (res.url.indexOf('code') !== -1) {

                const urlParams = new URL(res.url);
                const code =  urlParams.searchParams.get('code');
                if (this.code !== '') {
                    iapReturn.close();
                    this.exchangeCode(code, true);
                }
            }
        });
    }

    async exchangeCode(loginCode, redirect = false) {

        if (loginCode !== null && !this.authService.isLoggedIn()) {
            this.logginIn = true;
            const provider = JSON.parse(localStorage.getItem('current_provider'));
            const data = await this.authService.getTokensByCode(provider, loginCode);
            if (data.error) {
                this.snackBar.open(data.message, '', {
                    duration: 4000,
                });
            }
        }
        this.logginIn = false;
        if (redirect) {

            this.authService.isLoggedIn();
            await this.userService.getUserData();
            this.Zone.run(() => {
                this.router.navigateByUrl('/home');
            });
        }
    }

}
