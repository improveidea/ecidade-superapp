import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule, MatTable } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';

import { CamposModule } from '../campos/campos.module';
import { TabelaComponent } from './components/tabela/tabela.component';
import { AnexoComponent as SecaoAnexosComponent } from './components/anexo/anexo.component';

@NgModule({
  declarations: [
    TabelaComponent,
    SecaoAnexosComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatIconModule,
    FlexLayoutModule,
    CamposModule
  ],
  exports: [
    CamposModule,
    TabelaComponent,
    SecaoAnexosComponent
  ]
})
export class SecoesModule { }
