import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

import {StateService} from '../../../services/state.service';
import {ApiService} from '../../../services/api/api.service';
import {FormsDataService} from '../../../solicitacao-atendimento/services/forms-data.service';

@Component({
  selector: 'app-secao-anexo',
  templateUrl: './anexo.component.html',
  styleUrls: ['./anexo.component.scss']
})
export class AnexoComponent implements OnInit {

  @Input() secao: any;
  @Input() campos: any[];
  @Input() form: FormGroup;
  @Input() readonly: boolean;
  @Input() secoes: any[];
  @Input() dadosSecoesTabela: BehaviorSubject<Map<string, any>>;
  @Input() cleanedForm: BehaviorSubject<boolean>;
  @Input() nextStepClick: BehaviorSubject<boolean>;

  public camposAnexos: any[];

  constructor(
    private readonly api: FormsDataService,
    private readonly stateService: StateService
  ) {
  }

  ngOnInit() {

    this.camposAnexos = this.campos.slice(0);

    this.dadosSecoesTabela.asObservable().subscribe(secoesTabela => {
      if (this.secao.endpoint) {
        // eslint-disable-next-line no-underscore-dangle
        this._carregarCamposProcesso(this.secao.endpoint);
      }

      if (!secoesTabela || secoesTabela.size < 1) {
        return;
      }

      this.buscarCodigosAtividades(secoesTabela);
    });
  }

  private _carregarCamposProcesso(endpoint: any) {

    if (!this.stateService.getValue().tipo || !this.stateService.getValue().tipo.id) {
      return;
    }

    this.carregarCampo(endpoint, this.stateService.getValue().tipo.id);
  }

  private buscarCodigosAtividades(secoesTabela: any) {

    this.camposAnexos = this.campos.slice(0);

    if (this.secao.origens && this.secao.origens instanceof Array && this.secao.origens.length > 0) {

      this.secao.origens.forEach(origem => {

        if (origem.secao) {

          const secao = this.secoes.find(secaoObj => secaoObj.nome === origem.secao);
          let values = null;

          if (secao.tipo === 'tabela') {

            const itens = secoesTabela.get(origem.secao);
            values = [];

            if (itens && itens.size > 0) {

              itens.forEach(item => {

                let value = item[origem.campo];

                secao.campos.forEach(campo => {

                  if (campo.nome === origem.campo) {

                    switch (campo.tipo) {
                      case 'autocomplete':
                      case 'lista':
                      case 'lista_dinamica':
                        value = value.id ? value.id : value.codigo;
                        break;

                      case 'date':
                      case 'data':
                        value = value.format('L');
                        break;
                    }
                  }
                });

                values.push(value);
              });
            }
          } else {
            values = this.form.get(origem.secao).get(origem.campo);
          }

          if (values && !(values instanceof Array)) {
            values = [values];
          }

          this.carregarCamposAtividades(origem, values);
        }
      });
    }
  }

  private carregarCamposAtividades(origem: any, values: any[]) {

    values.forEach(val => this.carregarCampo(origem.endpoint, val));
  }

  private carregarCampo(endpoint: any, value: any) {

    let path = endpoint.path;
    const qryParams = [];

    endpoint.params.forEach(param => {

      if (param.located === 'query') {
        qryParams.push(`${param.name}=${value}`);
      }

      if (param.located === 'path') {
        path = path.replace('{' + param.name + '}', value);
      }
    });

    if (qryParams.length > 0) {
      path += `?` + qryParams.join('&');
    }

    if (endpoint.method === 'GET') {
      this.api.get(path).subscribe(result => {
        result.forEach(documentoAtividade => {
          documentoAtividade.flexSize = this.ajustarTamanhoCampo(documentoAtividade);
          this.camposAnexos.push(documentoAtividade);
        });

        const formGroup = {};

        this.camposAnexos.forEach(campoAnexo => {
          if (campoAnexo.obrigatorio === true) {
            formGroup[campoAnexo.nome] = new FormControl('', [Validators.required]);
          } else {
            formGroup[campoAnexo.nome] = new FormControl('');
          }
        });

        this.form.setControl(this.secao.nome, new FormGroup(formGroup));
      });
    }
  }

  private ajustarTamanhoCampo(campo: any): string {

    const tamanho = campo.tamanho;
    if (tamanho === 100) {
      return '100%';
    }

    return `${tamanho}% - 11px`;
  }
}
