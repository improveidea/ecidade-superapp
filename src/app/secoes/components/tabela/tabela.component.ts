import {Component, ViewChild, Input, Output, OnInit, AfterViewInit} from '@angular/core';
import {EventEmitter} from '@angular/core';
import {FormGroup} from '@angular/forms';
import { MatTable } from '@angular/material/table';
import {BehaviorSubject} from 'rxjs';
import {RequisicoesCamposService} from 'src/app/campos/services/requisicoes-campos.service';
import {ApiSelectService} from 'src/app/services/api.select.service';
import {AtributoService} from 'src/app/services/atributo.service';

const INITAL_DATA_ITEM: any = {};

export interface ColunaTabela {
  label: string;
  name: string;
}

@Component({
  selector: 'app-secao-tabela',
  templateUrl: './tabela.component.html',
  styleUrls: ['./tabela.component.scss']
})
export class TabelaComponent implements OnInit, AfterViewInit {

  @Input() form: FormGroup;
  @Input() secao: any;
  @Input() campos: any;
  @Input() tableDataOld: any = [];
  @Input() cleanedForm: BehaviorSubject<boolean>;
  @Input() erroTabela: BehaviorSubject<any>;
  @Output() getValues: EventEmitter<any>;
  @ViewChild(MatTable, {static: true}) table: MatTable<any>;

  public tableColumns: ColunaTabela[] = [];
  public visibleColumns: string[] = [];
  public tableData: any[] = [];
  public tableError: string | null = null;

  private tableDataValues: BehaviorSubject<Map<number, any>> = new BehaviorSubject(new Map());
  private tableContent: BehaviorSubject<Map<number, any>> = new BehaviorSubject(new Map());
  private lastItemID = 0;

  constructor(
    private readonly requisicoesCampos: RequisicoesCamposService,
    private readonly apiSelectService: ApiSelectService,
    private readonly atributoService: AtributoService,
  ) {
    this.getValues = new EventEmitter<any>();
  }
  get secaoControl() {
    return this.form.get(this.secao.nome);
  }

  ngOnInit() {

    this.visibleColumns.push('codigo');
    this.tableColumns.push({
      label: 'codigo',
      name: 'codigo',
    });

    this.campos.forEach(campo => {

      const column = {
        label: campo.label,
        name: campo.nome,
      };

      // tslint:disable-next-line:triple-equals
      if (campo.mostrarNaTabela === undefined || campo.mostrarNaTabela === true) {
        this.visibleColumns.push(column.name);
      }

      this.tableColumns.push(column);
    });

    this.visibleColumns.push('actions_column');

    this.tableColumns.forEach(tableColumn => INITAL_DATA_ITEM[tableColumn.name] = null);
    this.tableData.push(INITAL_DATA_ITEM);

    this.cleanedForm.asObservable().subscribe(cleaned => cleaned === true ? this.clear() : false);
    this.erroTabela.asObservable().subscribe(erro => {

      if (erro == null) {
        return this.tableError = null;
      }

      // tslint:disable-next-line:triple-equals
      if (this.secao.nome === erro.secao) {
        this.tableError = erro.erro;
      }
    });
    if (this.secao.hasOwnProperty('carregaDados')) {
      if (this.secao.carregaDados.hasOwnProperty('endpoint')) {
        const endpoint = this.secao.carregaDados.endpoint;

        if (endpoint.params) {
          this.atributoService.receiveFieldValueChanged().subscribe((response) => {
            // tslint:disable-next-line:triple-equals
            const secao = endpoint.params.find(param => param.secao === response.section);

            if (secao) {
              // tslint:disable-next-line:triple-equals
              const campo = endpoint.params.find(param => param.campo === response.field);

              if (campo) {
                this.carregaDados();
              }
            }
          });
        } else {
          this.carregaDados();
        }
      }
    }
  }

  ngAfterViewInit() {

    this.tableContent.asObservable().subscribe(response => {
      this.tableData = [];

      if (response.size < 1) {
        this.tableData.push(INITAL_DATA_ITEM);
        this.lastItemID = 0;
      }

      response.forEach(item => this.tableData.push(item));
      this.refreshTable();
    });

    this.tableDataValues.asObservable().subscribe(response => {
      this.getValues.emit({
        secao: this.secao.nome,
        valores: response
      });
    });


    if (this.tableDataOld.length > 0) {
      this.oldItens(this.tableDataOld);
    }

  }

  getFormatedValueFromField(field, value) {

    switch (field.tipo) {
      case 'data':
      case 'date':
        return value ? value.format('L') : null;
        break;

      case 'lista':
        // tslint:disable-next-line:triple-equals
        return value ? field.opcoes.filter(item => (item.codigo) ? item.codigo == value : item.id == value).shift().descricao : null;
        break;

      case 'lista_dinamica':
        const fieldOptions = this.requisicoesCampos.getValue(field.nome, 'opcoes');
        // tslint:disable-next-line:triple-equals
        return value ? fieldOptions.filter(item => (item.codigo) ? item.codigo == value : item.id == value).shift().descricao : null;
        break;

      case 'autocomplete':
        return value ? value.descricao : null;
        break;
    }

    return value;
  }

  disableValidations() {

    this.campos.forEach(c => {
      this.secaoControl.get(c.nome).clearValidators();
      this.secaoControl.get(c.nome).updateValueAndValidity();
    });
  }

  addItem(ev): void {

    if (!this.validateFormData()) {
      return;
    }

    this.carregaTabela(null);
  }

  carregaTabela(dados: any) {
    const linha = {
      codigo: ++this.lastItemID
    };
    const linhaValue = {...linha};

    this.campos.forEach(c => {
      linha[c.nome] = this.getFormatedValueFromField(c, (dados ? dados[c.nome] : this.secaoControl.get(c.nome).value));
      linhaValue[c.nome] = (dados ? dados[c.nome] : this.secaoControl.get(c.nome).value);
    });

    if (!this.validateTable(linha)) {
      this.lastItemID--;
      return;
    }

    this.tableError = null;

    const mapTableContent = this.tableContent.getValue();
    const mapTableDataValues = this.tableDataValues.getValue();

    mapTableContent.set(linha.codigo, linha);
    mapTableDataValues.set(linhaValue.codigo, linhaValue);

    this.tableContent.next(mapTableContent);
    this.tableDataValues.next(mapTableDataValues);
    this.clearForm();
  }

  oldItens(array): void {
    const me = this;
    array.values.forEach(elem => {
      const linha = {codigo: ++me.lastItemID};
      const linhaValue = {...linha};
      const mapTableContent = me.tableContent.getValue();
      const mapTableDataValues = me.tableDataValues.getValue();
      mapTableContent.set(linha.codigo, array.content[linha.codigo - 1]);
      mapTableDataValues.set(linhaValue.codigo, elem);
      me.tableContent.next(mapTableContent);
      me.tableDataValues.next(mapTableDataValues);

    });
  }

  dropItem(idLinha: number): void {

    const mapTableContent = this.tableContent.getValue();
    const mapTableDataValues = this.tableDataValues.getValue();

    mapTableContent.delete(idLinha);
    mapTableDataValues.delete(idLinha);

    if (mapTableContent.size < 1) {
      this.clearForm();
      this.validateFormData();
    }

    this.tableContent.next(mapTableContent);
    this.tableDataValues.next(mapTableDataValues);
  }

  hasError(): boolean {
    return this.tableError == null ? false : true;
  }

  getError(): string | null {
    return this.tableError;
  }

  private carregaDados() {
    const endpoint = this.secao.carregaDados.endpoint;

    let path = endpoint.path;

    if (endpoint.params) {
      let params = endpoint.params.map((param) => {
        let value = param.valor;

        if (!value) {
          value = this.form.get(param.secao || this.secao.nome).get(param.campo).value;
        }

        return `${param.nome}=${value}`;
      });

      params = (params.length > 0 ? `?${params.join('&')}` : '');

      path = path + params;
    }

    this.apiSelectService.api(endpoint.local).get(path).subscribe(response => {
      const dados = this.apiSelectService.response(response);

      if (dados instanceof Array) {
        dados.forEach((dado) => {
          this.carregaTabela(dado);
        });
      } else {
        this.carregaTabela(dados);
      }
    });
  }

  private refreshTable() {
    this.table.renderRows();
  }
  private clearForm() {
    this.campos.forEach(campo => this.secaoControl.get(campo.nome).reset());
  }

  private validateFormData() {

    let valid = true;

    for (const campo of this.campos) {
      const val = this.getFormatedValueFromField(campo, this.secaoControl.get(campo.nome).value);

      if (campo.obrigatorio) {

        if (val == null || (typeof val === 'string' && val.trim() === '')) {
          this.secaoControl.get(campo.nome).markAsTouched();
          valid = false;
        }
      }
    }

    return valid;
  }

  private validateTable(item: any) {

    if (!this.validateTableUnit(item)) {
      return false;
    }

    return true;
  }

  private validateTableUnit(item: any): boolean {

    let valid = true;
    let qteEncontrado = 0;
    let mensagem: string = null;
    const mapTableContent = this.tableContent.getValue();
    const tableContent = [];

    mapTableContent.forEach(itemTable => {
      tableContent.push(itemTable);
    });

    if (this.secao.unicidade && this.secao.unicidade.length > 0) {

      this.secao.unicidade.forEach(campoUnicidade => {

        const valorCampo = item[campoUnicidade.campo];
        // tslint:disable-next-line:triple-equals
        const itemEncontrado = tableContent.find(itemTable => itemTable[campoUnicidade.campo] == valorCampo);

        switch (campoUnicidade.tipo) {
          case 'campo':
            if (itemEncontrado) {
              valid = false;
            }
            break;

          case 'quantidade':
            const valoresAceitos = campoUnicidade.valor;

            // tslint:disable-next-line:triple-equals
            if (valoresAceitos.find(valor => valor == valorCampo)) {

              tableContent.forEach(itemTable => {
                // tslint:disable-next-line:triple-equals
                qteEncontrado += itemTable[campoUnicidade.campo] == valorCampo ? 1 : 0;
              });

              if (qteEncontrado >= campoUnicidade.quantidade) {
                valid = false;
              }
            }
            break;
        }

        if (valid === false && mensagem == null) {
          mensagem = campoUnicidade.mensagem;
        }
      });
    }

    if (mensagem != null) {
      this.tableError = mensagem;
    }

    return valid;
  }

  private clear(): void {

    const mapTableContent = this.tableContent.getValue();
    const mapTableDataValues = this.tableDataValues.getValue();

    mapTableContent.clear();
    mapTableDataValues.clear();

    this.tableContent.next(mapTableContent);
    this.tableDataValues.next(mapTableDataValues);

    this.lastItemID = 0;
    this.tableError = null;
  }


}
