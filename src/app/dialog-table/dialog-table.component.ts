import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  templateUrl: './dialog-table.component.html',
  styleUrls: ['./dialog-table.component.scss']
})
export class DialogTableComponent implements OnInit {
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<object>;
  public dataTable: object[] = [];
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public pageSize = 5;
  public dialog: any;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    @Inject(MAT_DIALOG_DATA) public readonly data,
    private dialogRef: MatDialogRef<DialogTableComponent>,
  ) { }

  ngOnInit() {
    this.dialog = this.data.field.change.dialog;

    this.displayedColumns = this.dialog.columns.map(column => column.name);
    this.pageSize = (this.dialog.initialQuantity || 5);

    this.loadResponse();
  }

  loadResponse() {
    this.dataTable = this.data.response;

    this.dataSource = new MatTableDataSource(this.dataTable);

    if (this.data.response.length > 100) {
      this.pageSizeOptions.push(this.data.response.length);
    }

    this.dataSource.paginator = this.paginator;
  }

  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selected(row) {
    this.dialogRef.close(row);
  }

  close() {
    this.dialogRef.close(null);
  }
}
