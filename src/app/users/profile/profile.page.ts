import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {AlertController, CheckboxCustomEvent, ModalController, ToastController} from "@ionic/angular";
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  user: any
  handlerMessage = '';
  roleMessage = '';

  public canDismiss = false;
  maria = false

  presentingElement = null;

  @Input() modal

  constructor(protected userService: UserService,
              private alertController: AlertController,
              private toastController: ToastController,
              private router: Router,
              private modalCtrl: ModalController
  ) {
  }

  async ngOnInit() {

    this.presentingElement = document.querySelector('.ion-page');
    const user = await this.userService.getUserData(false)
    if (!user.data) {
      return;
    }

    this.user = user.data;
  }

  async cancelAccount() {

    if (!this.canDismiss) {
      this.notifyToast('marque a confirmação de exclusão.')
      return false;
    }
    const alert = await this.alertController.create({
      header: 'voce realmente confirma a exclusão de sua conta?',
      buttons: [
        {
          text: 'Manter',
          role: 'cancel',
          handler: () => {
            this.handlerMessage = 'Alert canceled';
          },
        },
        {
          text: 'Confirmar',
          role: 'confirm',
          handler: () => {
            this.handlerMessage = 'Alert confirmed';
          },
        },
      ],
    });

    await alert.present();

    const {role} = await alert.onDidDismiss();
    if (role != 'confirm') {
      return
    }
    let cancel = this.userService.cancelAccount()
    if (!cancel) {
      return;
    }
    const toast = await this.toastController.create({
      message: 'Conta cancelada com sucesso.',
      icon: 'alert-circle-outline',
      duration: 3000,
      position: 'top'

    });
    toast.present();
    localStorage.clear();
    sessionStorage.clear();
    this.canDismiss = true;
    await this.modalCtrl.dismiss();
    await this.router.navigateByUrl('/');

  }


  async exit() {
    const toast = await this.toastController.create({
      message: 'Deseja sair de sua conta?',
      duration: 3000,
      buttons: [
        {
          side: 'end',
          icon: 'close',
          text: 'Sim',
          handler: () => {
            localStorage.clear();
            sessionStorage.clear();
            this.router.navigateByUrl('/');
          }
        }
      ]
    });
    toast.present();
  }

  onTermsChanged(event: Event) {

    const ev = event as CheckboxCustomEvent;
    this.canDismiss = ev.detail.checked;
    this.maria = ev.detail.checked

  }

  async notifyToast(message) {
    const toast = await this.toastController.create({
      message: message,
      icon: 'alert-circle-outline',
      duration: 3000,
      position: 'top'

    });
    toast.present();
  }

}
