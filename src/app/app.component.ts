import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from './services/auth/auth.service';
import {Subscription} from 'rxjs';
import {IonRouterOutlet, MenuController, Platform, ToastController} from '@ionic/angular';
import {LoaderService} from './services/loader-service/loader-service';
import {UserService} from './services/user/user.service';
import {Router} from '@angular/router';
// import {App} from '@capacitor/app';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import {ApiService} from "./services/api/api.service";
const w: any = window;
@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
    hasRequest = false;
    @ViewChild(IonRouterOutlet, {static: true}) routerOutlet: IonRouterOutlet;
    public appPages = [
        {title: 'Início', url: '/home', icon: 'home'},
        {title: 'Cidades', url: '/city-list', icon: 'paper-plane'},
        {title: 'Perfil', url: '/me-profile', icon: 'person'},
    ];
    userSubscription = new Subscription();
    isAuthenticated$ = new Subscription();
    isLogged = false;
    user = {
        name: '',
        email: '',
        current_city: {
            name: '',
            state: {
                short: ''
            }
        }
    };
    city = '';

    constructor(
        private platform: Platform,
        private authService: AuthService,
        private loaderService: LoaderService,
        private toastController: ToastController,
        private userService: UserService,
        private router: Router,
        private fcm: FCM,
        private apiService: ApiService,
        private menu: MenuController) {
        this.userSubscription = this.authService.userSubject.subscribe(message => {
            if (message.user === null || message.user === {}) {
                return;
            }
            this.user = message.user;
            this.city = 'Nenhuma cidade selecionada';
            if (this.user.current_city) {
                this.city = `${this.user.current_city.name} - ${this.user.current_city.state.short}`;
            }

        });
        this.isAuthenticated$ = this.authService.isAuthenticated.subscribe(authenticaded => {
            this.isLogged = authenticaded;
        });
        this.initializeApp();
        this.loaderService.activeSubject$.subscribe(active => {
            this.hasRequest = active;
        });
    }

    initializeApp() {


        this.platform.ready().then(() => {
            this.fcm.onNotification().subscribe(data => {
                if (!data.wasTapped) {
                    this.notifyToast(data.body);
                };
            });
            this.fcm.onTokenRefresh().subscribe(fcmToken => {

                const data = {
                    token: fcmToken,
                    system: 1
                };
                this.apiService.post('me/devices-token', data).subscribe(response => {
                    console.log(response);
                });
            });
            const response = this.platform.backButton.subscribeWithPriority(-1, () => {
                const urlActive = this.router.url.trim();
                if (urlActive === '/home' || urlActive === ' /home') {
                    navigator['app'].exitApp();
                }
            });
        });
    }

    ngOnInit() {
        this.userService.getUserData(false);
        this.menu.enable(true, 'main');
        this.menu.swipeGesture(true, 'main');
        this.menu.open('main');
    }

    async exit() {
        const toast = await this.toastController.create({
            message: 'Deseja sair de sua conta?',
            duration: 3000,
            buttons: [
                {
                    side: 'end',
                    icon: 'close',
                    text: 'Sim',
                    handler: () => {
                        this.menu.close('main');
                        this.menu.swipeGesture(false, 'main');
                        localStorage.clear();
                        sessionStorage.clear();
                        this.router.navigateByUrl('/');
                    }
                }
            ]
        });
        toast.present();
    }

    async notifyToast(message) {
        const toast = await this.toastController.create({
            message: message,
            icon: 'alert-circle-outline',
            duration: 3000,
            position: 'top'

        });
        toast.present();
    }
    isLoggedIn() {
        return this.isLogged;
    }
}
