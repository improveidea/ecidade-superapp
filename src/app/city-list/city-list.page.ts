import {Component, NgZone, OnInit} from '@angular/core';
import {CityListService} from './city-list.service';
import {Router} from '@angular/router';
import {UserService} from '../services/user/user.service';

@Component({
    selector: 'app-city-list',
    templateUrl: './city-list.page.html',
    styleUrls: ['./city-list.page.scss'],
})
export class CityListPage implements OnInit {

    cities = [];
    currentCity;

    constructor(private cityService: CityListService, private router: Router, private userService: UserService,
                private zone: NgZone) {
    }

    async ngOnInit() {
        this.cities = await this.cityService.getCities();
    }

    gotoCity(city) {
        this.router.navigateByUrl(`/first-access-city/${city.id}`);
    }

    async setCity(city) {
        this.currentCity = await this.userService.setCurrentCity(city);
        const user = await this.userService.getUserData();

        setTimeout(() => {
            this.zone.run(() => this.router.navigateByUrl(`/home`));
        }, 500);
    }

}
