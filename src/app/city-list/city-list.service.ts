import { Injectable } from '@angular/core';
import {ApiService} from '../services/api/api.service';

@Injectable({
  providedIn: 'root'
})
export class CityListService {

  constructor(private apiService: ApiService) {
  }

  async getCities()
  {
     const cities = await this.apiService.get('cities/integration?include=has-city').toPromise();
     if (cities.error) {
       return [];
     }

     return cities.data;
   }

    async getCitiesById(id)
    {
        const cities = await this.apiService.get(`cities/${id}`).toPromise();
        if (cities.error) {
            return null;
        }
        return cities.data;
    }
}
