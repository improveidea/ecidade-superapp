import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {CpfCnpjValidator} from '../validators/CpfCnpjValidator';
import {ApiService} from '../services/api/api.service';
import {ToastController} from '@ionic/angular';

@Component({
  selector: 'app-first-access',
  templateUrl: './first-access.page.html',
  styleUrls: ['./first-access.page.scss'],
})


export class FirstAccessPage implements OnInit {

  public formGroup: FormGroup;
  public name: FormControl;
  public email: FormControl;
  public document: FormControl;
  public password: FormControl;
  public repassword: FormControl;

  constructor(public apiService: ApiService, public toast: ToastController) {
  }

  ngOnInit() {

    this.name = new FormControl('', [Validators.required]);
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.document = new FormControl('', [CpfCnpjValidator.validate, Validators.required]);
    this.password = new FormControl('', [Validators.required]);
    this.repassword = new FormControl('', [Validators.required]);
    this.formGroup = new FormGroup( {
      name : this.name,
      email: this.email,
      document: this.document,
      password: this.password,
      repassword: this.repassword,
    });
  }

  public async save() {

    if (!this.formGroup.valid) {
      this.presentToast('Verfique os campos em vermelho!');
      return;
    }
    const data = {
      name: this.name.value,
      email: this.email.value,
      document: this.document.value,
      password: this.password.value,
      repassword: this.repassword.value
    };

    await this.apiService.post('eauth/user', data).subscribe(response => {
      if (response.eauth && response.eauth.message) {
        this.presentToast(response.eauth.message, 3000);
        if (response.eauth.success) {
          this.formGroup.reset();
          this.name.setErrors(null);
          this.email.setErrors(null);
          this.document.setErrors(null);
          this.password.setErrors(null);
          this.repassword.setErrors(null);
        }
      }
    });
  }

  async presentToast(msg, duration = null) {
    const toast = await this.toast.create({
      message: msg,
      duration: duration || 2000
    });
    toast.present();
  }

}
