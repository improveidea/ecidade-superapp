import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FirstAccessPageRoutingModule } from './first-access-routing.module';

import { FirstAccessPage } from './first-access.page';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {PageHeaderModule} from '../components/page-header/page-header.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FirstAccessPageRoutingModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        ReactiveFormsModule,
        MatDividerModule,
        PageHeaderModule
    ],
    declarations: [FirstAccessPage]
})
export class FirstAccessPageModule {}
