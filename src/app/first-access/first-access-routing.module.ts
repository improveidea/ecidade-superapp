import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirstAccessPage } from './first-access.page';

const routes: Routes = [
  {
    path: '',
    component: FirstAccessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FirstAccessPageRoutingModule {}
