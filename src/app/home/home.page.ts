import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api/api.service';
import {Router} from '@angular/router';
import {ToastController} from '@ionic/angular';
import {AndroidPermissions} from "@ionic-native/android-permissions/ngx";
import {Platform} from '@ionic/angular';
import {FCM} from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

    dashboards = [];

    constructor(
        public api: ApiService,
        private router: Router,
        private toastController: ToastController,
        private platform: Platform,
        private fcm: FCM,
        private androidPermissions: AndroidPermissions,
    ) { }

    ngOnInit() {
        const isCordova = this.platform.is('cordova');
        if (!isCordova) {
            return;
        }
        this.fcm.requestPushPermission().then(() => {
            this.fcm.getToken().then(fcmToken => {
                const data = {
                    token: fcmToken,
                    system: 1
                };
                this.api.post('me/devices-token', data).subscribe(response => {
                    console.log(response);
                });
            });
        });
        this.checkPermissions();
    }

    ionViewWillEnter() {
        this.loadDashboard();
    }

    async loadDashboard() {
        const data = JSON.parse(await localStorage.getItem('user_data'));
        if (data.current_city === null) {
            this.forceUserSelectCity();
            return;
        }
        if (data.current_city.has_permission) {
            this.api.get('dashboard').subscribe(response => {
                this.dashboards = response.dashboards;
            });
        } else {
            this.presentToast('Sem acesso nesse municipio. Favor solicitar acesso.');
        }
    }

    forceUserSelectCity() {
        this.router.navigateByUrl('city-list');
    }

    async presentToast(msg, duration = null) {
        const toast = await this.toastController.create({
            message: msg,
            duration: duration || 2000
        });
        toast.present();
    }

  private checkPermissions() {

    const permissionsToCheck = [
      this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
    ];

    permissionsToCheck.map(permission => {
      this.androidPermissions.checkPermission(permission).then(
          response => {
            if (!response.hasPermission) {
              this.androidPermissions.requestPermission(permission);

            }
          }
      );
    });
  }
}
