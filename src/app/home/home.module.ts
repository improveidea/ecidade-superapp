import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import {PageHeaderModule} from '../components/page-header/page-header.module';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatGridListModule} from '@angular/material/grid-list';
import {ServicesComponent} from '../components/card/services/services.component';
import {LoggedInComponent} from "../components/card/logged-in/logged-in.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomePageRoutingModule,
        PageHeaderModule,
        MatCardModule,
        MatIconModule,
        MatGridListModule
    ],
    declarations: [HomePage, ServicesComponent, LoggedInComponent]
})
export class HomePageModule {}
