import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProcessPageRoutingModule } from './process-routing.module';

import { ProcessPage } from './process.page';
import {PageHeaderModule} from '../../components/page-header/page-header.module';
import {ProcessComponent} from '../../components/card/process/process.component';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {NoInformationComponent} from '../../components/no-information/no-information.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProcessPageRoutingModule,
        PageHeaderModule,
        MatCardModule,
        MatGridListModule,
        MatIconModule
    ],
    exports: [
        ProcessComponent,
        NoInformationComponent
    ],
    declarations: [ProcessPage, ProcessComponent, NoInformationComponent]
})
export class ProcessPageModule {}
