import { Component, OnInit } from '@angular/core';
import {FormsDataService} from '../../../solicitacao-atendimento/services/forms-data.service';
import {StateService} from '../../../services/state.service';
import {Router} from '@angular/router';
import {InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser/ngx';
import {UserService} from "../../../services/user/user.service";
@Component({
  selector: 'app-new-process',
  templateUrl: './new-process.page.html',
  styleUrls: ['./new-process.page.scss'],
})
export class NewProcessPage implements OnInit {

  types;
  constructor(private formDataService: FormsDataService, private state: StateService, private router: Router,
              private inAppBrowser: InAppBrowser,
              private userService: UserService) { }

  ngOnInit() {
    this.getTypes();
  }

  async getTypes() {
    const document = this.userService.getDocument();
    const types = await this.formDataService.get(`solicitacoes/menu?formareclamacao=6&cpf_cnpj=${document}`).toPromise();
    this.types = types.menu
  }

  gotToProcess(process) {
    this.state.setValue('tipo', process);
    this.router.navigateByUrl('/home/process/new-process-form');
  }
  showHelp(link) {
    if (link === '') {
      return;
    }
    const options: InAppBrowserOptions = {
      toolbarposition: 'top',
      location: 'yes', // Or 'no'
      clearcache: 'yes',
      clearsessioncache: 'yes',
      hideurlbar: 'no',
      zoom: 'no', // Android only ,shows browser zoom controls
      toolbar: 'yes', // iOS only
    };
    const iapReturn = this.inAppBrowser.create(link, '_system', options);
  }
}
