import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewProcessPageRoutingModule } from './new-process-routing.module';

import { NewProcessPage } from './new-process.page';
import {PageHeaderModule} from '../../../components/page-header/page-header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewProcessPageRoutingModule,
    PageHeaderModule,
  ],
  declarations: [NewProcessPage]
})
export class NewProcessPageModule {}
