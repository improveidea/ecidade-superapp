import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewProcessPage } from './new-process.page';

const routes: Routes = [
  {
    path: '',
    component: NewProcessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewProcessPageRoutingModule {}
