import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewProcessFormPageRoutingModule } from './new-process-form-routing.module';

import { NewProcessFormPage } from './new-process-form.page';
import {PageHeaderModule} from '../../../components/page-header/page-header.module';
import {SolicitacaoAtendimentoModule} from '../../../solicitacao-atendimento/solicitacao-atendimento.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewProcessFormPageRoutingModule,
    PageHeaderModule,
    SolicitacaoAtendimentoModule
  ],
  declarations: [NewProcessFormPage]
})
export class NewProcessFormPageModule {}
