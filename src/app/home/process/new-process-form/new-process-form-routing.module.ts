import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewProcessFormPage } from './new-process-form.page';

const routes: Routes = [
  {
    path: '',
    component: NewProcessFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewProcessFormPageRoutingModule {}
