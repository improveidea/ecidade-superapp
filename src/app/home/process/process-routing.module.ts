import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProcessPage } from './process.page';

const routes: Routes = [
  {
    path: '',
    component: ProcessPage
  },
  {
    path: 'info/:id',
    loadChildren: () => import('./info/info.module').then( m => m.InfoPageModule)
  },
  {
    path: 'info/:id/chat',
    loadChildren: () => import('./info/chat/chat.module').then( m => m.ChatPageModule)
  },
  {
    path: 'new-process',
    loadChildren: () => import('./new-process/new-process.module').then( m => m.NewProcessPageModule)
  },
  {
    path: 'new-process-form',
    loadChildren: () => import('./new-process-form/new-process-form.module').then( m => m.NewProcessFormPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProcessPageRoutingModule {}
