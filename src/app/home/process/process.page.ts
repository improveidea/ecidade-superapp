import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {Router} from '@angular/router';

export interface Process {
  title: string;
  // eslint-disable-next-line id-blacklist
  number: number;
  year: number;
  id: string;
  date: string;
}


@Component({
  selector: 'app-process',
  templateUrl: './process.page.html',
  styleUrls: ['./process.page.scss'],
})
export class ProcessPage implements OnInit {

  hasProcess = false;
  process = [];

  constructor(public api: ApiService, private router: Router) { }

  ngOnInit() {
    this.hasProcess = false;
    this.loadProcess();
  }

  async loadProcess() {
    await this.api.get('process').subscribe(response => {
      response.process.map(theProcess => {
        this.process.push({
          title: theProcess.title,
          date: theProcess.date,
          year: theProcess.year,
          code: theProcess.code,
          id: theProcess.process_code,
          type: theProcess.type,
          status: theProcess.status,
          process_number: theProcess.process_number
        });
      });

      this.hasProcess = this.process.length > 0;
    });
  }
  newProcess()
  {
    this.router.navigateByUrl('/home/process/new-process');
  }
}
