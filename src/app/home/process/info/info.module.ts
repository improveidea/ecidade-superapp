import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfoPageRoutingModule } from './info-routing.module';

import { InfoPage } from './info.page';
import {PageHeaderModule} from '../../../components/page-header/page-header.module';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {ProcessPageModule} from "../process.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        InfoPageRoutingModule,
        PageHeaderModule,
        MatListModule,
        MatCardModule,
        MatGridListModule,
        MatIconModule,
        MatButtonModule,
        ProcessPageModule
    ],
    declarations: [InfoPage]
})
export class InfoPageModule {}
