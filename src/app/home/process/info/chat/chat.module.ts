import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ChatPageRoutingModule } from './chat-routing.module';

import { ChatPage } from './chat.page';
import {PageHeaderModule} from "../../../../components/page-header/page-header.module";
import {ProcessPageModule} from "../../process.module";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        ChatPageRoutingModule,
        PageHeaderModule,
        ProcessPageModule,
        MatIconModule
    ],
  declarations: [ChatPage]
})
export class ChatPageModule {}
