import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IonContent, LoadingController, Platform, ToastController} from "@ionic/angular";
import {ApiService} from "../../../../services/api/api.service";
import {ActivatedRoute} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import * as moment from 'moment';
import {AndroidPermissions} from "@ionic-native/android-permissions/ngx";
import {FileTransferObject} from "@ionic-native/file-transfer";
import {FileTransfer} from "@ionic-native/file-transfer/ngx";
import {File} from "@awesome-cordova-plugins/file/ngx";
import {InfoPage} from "../info.page";
import {FileOpener} from "@awesome-cordova-plugins/file-opener/ngx";
@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit, OnDestroy {
  messages = [];
  process:any;
  chatRequest = false;
  messageUser: FormControl;
  formGroupChat: FormGroup;
  oldMessageCount = 0;
  intervalMessages = null;
  intervalScroll = null
  id;

  constructor(
      private api: ApiService,
      private toast: ToastController,
      private route: ActivatedRoute,
      private androidPermissions: AndroidPermissions,
      private loadingController: LoadingController,
      private toastController: ToastController,
      private cordovaFileSystem: File,
      private platform: Platform,
      private fileOpener: FileOpener,
    ) {
    this.process = this.route.snapshot.queryParams;
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.messageUser = new FormControl('', [Validators.required]);
    this.formGroupChat = new FormGroup({
      messageUser: this.messageUser
    });

    this.loadMessages();
    this.chatRequest = true;
    this.intervalMessages = setInterval(() => {
      if (this.chatRequest) {
        this.loadMessages();
      }
    }, 5000);
  }

  ngOnDestroy() {
    this.chatRequest = false;
    if (this.intervalScroll) {
      clearInterval(this.intervalScroll);
    }
    if (this.intervalMessages) {
      clearInterval(this.intervalMessages);
    }
  }

  async loadMessages() {
    const response = await this.api.get(`process/${this.process.id}/messages`).toPromise();
    const receivedMessages = response.messages;
    let hasToScroll = false;
    if (this.oldMessageCount < receivedMessages.length) {
      this.messages = receivedMessages;
      this.oldMessageCount = receivedMessages.length;
      hasToScroll = true;
    }
    setTimeout(() => {
      if (this.chatRequest && hasToScroll) {
        document.getElementById('divSpacer').scrollIntoView();
      }
    }, 750);

  }

  async showAttachment() {
    const toast = await this.toast.create({
      message: 'O download dos arquivos devem ser feitos atrávés de um computador.',
      duration: 2500
    });
    toast.present();
  }

  async downloadAttachment(attach) {
    // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
    //     response => {
    //       if (!response.hasPermission) {
    //         this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
    //         return false;
    //       }
          this.executeDownload(attach);
        // }
    // );
  }

  public async executeDownload(attach) {

    const loading = await this.loadingController.create({
      message: 'Por favor, aguarde...',
      duration: 10000
    });
    await loading.present();

    let route = `process/${this.id}/attached-files?file=${attach[0].id_estorage}`;
    await this.api.get(route).subscribe(async response => {
      await loading.dismiss();
      if (response.file) {
        if (!response.file.data || !response.file.data[0].download) {
          const toast = await this.toastController.create({
            message: 'Não foi possível fazer o download do arquivo..',
            duration: 2000,
          });
          await toast.present();
          return;
        }

        const type = response.file.data[0].type;
        const mymeType = InfoPage.getMimesType(type);

        let fileSystemPath = `${this.cordovaFileSystem.externalRootDirectory}Download`;
        if (this.platform.is('ios')) {
          fileSystemPath = `${this.cordovaFileSystem.documentsDirectory}`;
        }

        const url = response.file.data[0].download;
        const explodeUrl = url.split('arquivo=');
        const nameFile = explodeUrl[1].trim();
        const fileTransfer: FileTransferObject = (new FileTransfer()).create();
        const filePath = `${fileSystemPath}/${nameFile}`;
        await fileTransfer.download(url, filePath);

        this.fileOpener.open(filePath, mymeType)
            .then(() => console.log('File is opened'))
            .catch(e => console.log('Error opening file', e));
        return;
      }
    });
  }

  sendMessage() {

    if (this.messageUser.value === '') {
      return false;
    }

    const data = {
      message: this.messageUser.value,
      attachments: []
    };
    const now = moment();
    const proxyMessage = {
      text: data.message,
      is_user: true,
      date : now.format('DD/MM/YYYY'),
      hour: now.format('hh:mm'),
      attachments: []
    };
    this.messages.push(proxyMessage);
    this.api.post(`process/${this.process.id}/messages/send`, data).subscribe(
        response => {
          this.formGroupChat.reset();
        }
    );
  }
}
