import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../services/api/api.service';
import {ActivatedRoute} from '@angular/router';
import {AlertController, LoadingController, Platform, ToastController} from '@ionic/angular';
import {FileTransfer} from '@ionic-native/file-transfer/ngx';
import {FileTransferObject} from '@ionic-native/file-transfer';
import {File} from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import mimes from '../../../core/variables/mimes';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';

@Component({
    selector: 'app-info',
    templateUrl: './info.page.html',
    styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

    public static defaultLengthDispachText = 40;

    titlePage = 'Aguarde...';
    id: number;
    process: any;
    details = [];

    currentFileDownload;
    currentFiles;
    currentDispatch;
    arquivo = '';

    hasProcess = false;

    showModalDownload = false;

    constructor(
        private api: ApiService,
        private route: ActivatedRoute,
        private toastController: ToastController,
        private alertController: AlertController,
        private loadingController: LoadingController,
        private cordovaFileSystem: File,
        private androidPermissions: AndroidPermissions,
        private fileOpener: FileOpener,
        private platform: Platform,
    ) {
    }

    ngOnInit() {
        const process = localStorage.getItem('process');
        this.id = this.route.snapshot.params.id;
        // this.process = this.route.snapshot.queryParams;
        this.process = JSON.parse(process);
        this.titlePage = `${this.process.code}/${this.process.year}`;
        this.loadProcess();
    }

    private async loadProcess() {
        await this.api.get(`process/${this.id}`).subscribe(response => {
            this.details = response.details.map(dispatch => {
                let dispatchText = dispatch.despacho;
                if (dispatchText.length > InfoPage.defaultLengthDispachText) {
                    dispatchText = dispatchText.substr(0, InfoPage.defaultLengthDispachText) + ' (...)';
                }
                dispatch.isShowComplete = false;
                dispatch.isShowPreview = true;
                dispatch.textDetail = dispatchText;

                if (!this.platform.is('ios') && dispatch.anexos.length > 1) {
                    const allId = dispatch.anexos.map(file => file.id_estorage);
                    dispatch.anexos.push({
                        'id_estorage': allId.join(','),
                        'descricao': 'Baixar todos os arquivos',
                        'all_files': true
                    });
                }
                return dispatch;
            });
            this.hasProcess = this.details !== null;
        });
    }

    public static getMimesType(type) {

        if (mimes[type]) {
            return mimes[type];
        }
        let mimeType = 'image/jpg';
        switch (type) {
            case 'pdf':
                mimeType = 'application/pdf';
                break;
            case 'txt':
                mimeType = 'application/txt';
                break;
            case 'xls':
            case 'xlsx':
                mimeType = 'application/xls';
                break;
            case 'csv':
                mimeType = 'application/csv';
                break;

            default:
                mimeType = 'image/jpg';
                break;
        }
        return mimeType;
    }

    setDispatch(dispatch) {
        this.currentDispatch = dispatch;
        this.details.map(detail => {
            detail.isShowPreview = true;
            detail.isShowComplete = dispatch === detail;
        });
        dispatch.isShowPreview = false;
    }

    async downloadFile(file) {
        this.currentFileDownload = file;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
            response => {
                if (!response.hasPermission) {
                    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
                    return false;
                }
                this.executeDownload();
            }
        );
    }

    private async executeDownload() {
        const loading = await this.loadingController.create({
            message: 'Por favor, aguarde...',
            duration: 10000
        });
        await loading.present();

        let route = `process/${this.id}/attached-files?file=${this.currentFileDownload.id_estorage}`;
        if (this.currentFileDownload.all_files) {
            route = `process/${this.id}/zip-attached-files?file=${this.currentFileDownload.id_estorage}`;
        }
        await this.api.get(route).subscribe(async response => {
            loading.dismiss();
            if (response.file) {
                if (!response.file.data || !response.file.data[0].download) {
                    const toast = await this.toastController.create({
                        message: 'Não foi possível fazer o download do arquivo..',
                        duration: 2000,
                    });
                    toast.present();
                    return;
                }

                if (this.currentFileDownload.all_files) {
                    const url = response.file.data[0].download;
                    const nameFile = response.file.data[0].name;

                    let fileSystemPath = `${this.cordovaFileSystem.externalRootDirectory}Download`;
                    const fileTransfer: FileTransferObject = (new FileTransfer()).create();
                    const filePath = `${fileSystemPath}/${nameFile}`;
                    await fileTransfer.download(url, filePath).then(async () => {
                        const alert = await this.alertController.create({
                            header: 'Aviso!',
                            subHeader: 'Download concluído!',
                            message: `O arquivo ${nameFile} está localizado na pasta Downloads do seu celular.`,
                            buttons: ['OK']
                        });
                        await alert.present();
                    });

                    return;
                }

                const type = response.file.data[0].type;
                const mymeType = InfoPage.getMimesType(type);

                let fileSystemPath = `${this.cordovaFileSystem.externalRootDirectory}Download`;
                if (this.platform.is('ios')) {
                    fileSystemPath = `${this.cordovaFileSystem.documentsDirectory}`;
                }

                const url = response.file.data[0].download;
                const explodeUrl = url.split('arquivo=');
                const nameFile = explodeUrl[1].trim();
                const fileTransfer: FileTransferObject = (new FileTransfer()).create();
                const filePath = `${fileSystemPath}/${nameFile}`;
                await fileTransfer.download(url, filePath);

                this.fileOpener.open(filePath, mymeType)
                    .then(() => console.log('File is opened'))
                    .catch(e => console.log('Error opening file', e));
                return;
            }
        });
    }


}
