import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../services/api/api.service";
import {ToastController} from "@ionic/angular";
import {Clipboard} from "@awesome-cordova-plugins/clipboard/ngx";
import {Router} from "@angular/router";
import {DebtsService} from "../../services/debts.service";

@Component({
    selector: 'app-debts',
    templateUrl: './debts.page.html',
    styleUrls: ['./debts.page.scss'],
})
export class DebtsPage implements OnInit {

    public totalDebts = 0.00;

    public debts = [];

    constructor(private api: ApiService,
                private toast: ToastController,
                private router: Router,
                private debtService: DebtsService,
                public clipboard: Clipboard) {
    }

    ngOnInit() {
        this.loadDebts();
        localStorage.removeItem('currentDebt');
    }

    async loadDebts() {
        await this.api.get('me/debts').subscribe(response => {
            this.debts = response.data.debts;
            this.totalDebts = response.data.total;
        });
    }

    async copyLine(debt) {
        const response = this.debtService.copyLine(debt);
    }

    openDetails(debt) {
        localStorage.setItem('currentDebt', JSON.stringify(debt));
        this.router.navigateByUrl('/home/debts/details');
    }
}
