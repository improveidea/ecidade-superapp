import {Component, Input, OnInit} from '@angular/core';
import {ToastController} from "@ionic/angular";
import {ActivatedRoute, Router} from "@angular/router";
import {DebtsService} from "../../../services/debts.service";

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  public debt = {
    title: '',
    due_date: '',
    uuid: '',
    total_value: 0.00,
    items: [],
  };

  constructor(private toast: ToastController,
              private debtService: DebtsService,
              private router: Router) { }

  ngOnInit() {
    const debt = localStorage.getItem('currentDebt');
    this.debt = JSON.parse(debt);
  }

  goBack() {
    localStorage.removeItem('currentDebt');
    this.router.navigateByUrl('/home/debts');
  }

  async copyLine() {
    const response = this.debtService.copyLine(this.debt);
  }

  async requestInstallment() {
    const toast = await this.toast.create({
      message: 'Vocês deve abrir um processo solicitando o parcelamento. Podemos enviá-lo para abrir um novo processo?',
      position: 'middle',
      buttons: [
        {
          side: 'end',
          text: 'Sim',
          handler: async () => {
              await this.router.navigateByUrl('/home/process/new-process');
          }
        }, {
          text: 'Não',
          role: 'cancel',
        }
      ]
    });
    await toast.present();
  }
}
