import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DebtsPageRoutingModule } from './debts-routing.module';

import { DebtsPage } from './debts.page';
import {PageHeaderModule} from "../../components/page-header/page-header.module";
import {MatCardModule} from "@angular/material/card";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {ProcessPageModule} from "../process/process.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DebtsPageRoutingModule,
        PageHeaderModule,
        MatCardModule,
        MatGridListModule,
        MatDividerModule,
        MatIconModule,
        MatButtonModule,
        ProcessPageModule
    ],
  declarations: [DebtsPage]
})
export class DebtsPageModule {}
