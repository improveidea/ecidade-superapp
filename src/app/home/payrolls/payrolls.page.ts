import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../services/api/api.service";
import {Router} from "@angular/router";
import {CalendarService} from "../../services/calendar.service";

@Component({
    selector: 'app-payrolls',
    templateUrl: './payrolls.page.html',
    styleUrls: ['./payrolls.page.scss'],
})


export class PayrollsPage implements OnInit {

    public payrolls = [];

    constructor(private api: ApiService, private router: Router, private calendarService: CalendarService) {
    }

    ngOnInit() {
        this.loadPayrolls().then();
        localStorage.removeItem('payroll');
        localStorage.removeItem('payrollDetail');
    }


    async loadPayrolls() {
        await this.api.get('payrolls').subscribe(response => {
            this.payrolls = response.data.payrolls;
            localStorage.setItem('payrolls', JSON.stringify(response.data.payrolls));
        });
    }


    openPayroll(payroll, payrollData) {
        localStorage.setItem('payroll', JSON.stringify(payroll));
        localStorage.setItem('payrollDetail', JSON.stringify(payrollData));
        this.router.navigateByUrl('/home/payrolls/details');
    }

    getMonthName(month) {
        return this.calendarService.getMonthName(month);
    }
}
