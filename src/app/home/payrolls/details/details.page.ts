import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {CalendarService} from "../../../services/calendar.service";
import {FileTransferObject} from "@ionic-native/file-transfer";
import {FileTransfer} from "@ionic-native/file-transfer/ngx";
import {File} from "@awesome-cordova-plugins/file/ngx";
import {FileOpener} from "@awesome-cordova-plugins/file-opener/ngx";
import {LoadingController, Platform, ToastController} from "@ionic/angular";
import {ApiService} from "../../../services/api/api.service";
import {InfoPage} from "../../process/info/info.page";

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  public titlePage:String = 'Aguarde...'

  public incomes = [];
  public discounts = [];
  public additionalInformation = [];

  public payroll;
  public payrollDetail;

  constructor(
      private router: Router,
      private calendarService: CalendarService,
      private cordovaFileSystem: File,
      private fileOpener: FileOpener,
      private platform: Platform,
      private loadingController: LoadingController,
      private api: ApiService,
      private toastController: ToastController,
  ) { }

  ngOnInit() {
    this.payroll = JSON.parse(localStorage.getItem('payroll'));
    this.payrollDetail = JSON.parse(localStorage.getItem('payrollDetail'));
    const month = this.calendarService.getMonthName(this.payroll.month)
    this.titlePage = `${month}/${this.payroll.year} - ${this.payrollDetail.payroll_type}`;


    this.payrollDetail.items.map(payrollDetail => {
      if (payrollDetail.type === 'rendimento') {
        this.incomes.push(payrollDetail)
      }
      if (payrollDetail.type === 'desconto') {
        this.discounts.push(payrollDetail)
      }
      if (payrollDetail.type === 'base') {
        this.additionalInformation.push(payrollDetail)
      }
    });
  }

  hasPaycheck() {
    return this.payrollDetail.paycheck_code !== null
  }

  async download() {

    const loading = await this.loadingController.create({
      message: 'Por favor, aguarde...',
      duration: 10000
    });
    await loading.present();

    // let route = `download/160`;
    let route = `download/${this.payrollDetail.paycheck_code}`;
    await this.api.get(route).subscribe(async response => {

      if (response.file) {
        if (!response.file.data || !response.file.data[0].download) {
          const toast = await this.toastController.create({
            message: 'Não foi possível fazer o download do arquivo..',
            duration: 2000,
          });
          toast.present();
          return;
        }

        const type = response.file.data[0].type;
        const mymeType = InfoPage.getMimesType(type);

        let fileSystemPath = `${this.cordovaFileSystem.externalRootDirectory}Download`;
        if (this.platform.is('ios')) {
          fileSystemPath = `${this.cordovaFileSystem.documentsDirectory}`;
        }

        const url = response.file.data[0].download;
        const explodeUrl = url.split('arquivo=');
        const nameFile = explodeUrl[1].trim();
        const fileTransfer: FileTransferObject = (new FileTransfer()).create();
        const filePath = `${fileSystemPath}/${nameFile}`;
        await fileTransfer.download(url, filePath);

        this.fileOpener.open(filePath, mymeType)
            .then(() => console.log('File is opened'))
            .catch(e => console.log('Error opening file', e));
        return;
      }
    });
  }
  goBack() {
    localStorage.removeItem('payroll');
    localStorage.removeItem('payrollDetail');
    this.router.navigateByUrl('/home/payrolls');
  }
}
