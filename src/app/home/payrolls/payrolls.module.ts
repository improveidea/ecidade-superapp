import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PayrollsPageRoutingModule } from './payrolls-routing.module';

import { PayrollsPage } from './payrolls.page';
import {PageHeaderModule} from "../../components/page-header/page-header.module";
import {ProcessPageModule} from "../process/process.module";
import {MatCardModule} from "@angular/material/card";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PageHeaderModule,
    PayrollsPageRoutingModule,
    ProcessPageModule,
    MatCardModule
  ],
  declarations: [PayrollsPage]
})
export class PayrollsPageModule {}
