import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import * as _ from 'lodash';

/**
 * Interface de definição do stado da aplicação
 */
export interface Attendance {
  step: number;
  dados: any;
  arquivos: any[];
  numeroAtendimento: number;
}

export const INITIAL: Attendance = {
  step: 0,
  dados: {},
  arquivos: [],
  numeroAtendimento: null,
};

export const PERSIST_KEY = 'attendance';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  private readonly attendance = new BehaviorSubject<Attendance>({...INITIAL});
  public readonly attendance$ = this.attendance.asObservable();

  constructor() {
  }

  get() {
    return this.attendance$;
  }

  set(attendance: Attendance) {
    Promise.resolve().then(() => {
      this.attendance.next({...attendance});
    });
  }

  getValue(key: keyof Attendance) {
    const attendance = this.attendance.getValue();
    return attendance[key] ? attendance[key] : null;
  }

  setValue(key: keyof Attendance, value: any) {
    const attendance = this.attendance.getValue();
    attendance[key] = value;
    this.set(attendance);
  }

  addValue(key: keyof Attendance, value: any) {
    const attendance = this.attendance.getValue();

    if (attendance[key] instanceof Array) {
      attendance[key].push(value);
      this.set(attendance);
    }
  }

  removeValue(key: keyof Attendance, value: any) {

    const attendance = this.attendance.getValue();

    if (attendance[key] instanceof Array) {

      const indice = attendance[key].indexOf(value);

      attendance[key].splice(indice, 1);

      this.set(attendance);
    }
  }

  clear() {
    this.set(INITIAL);
  }

  persist() {
    Promise.resolve().then(() => {
      const attendance = this.attendance.getValue();
      const stringify = JSON.stringify(attendance);
      sessionStorage.setItem(PERSIST_KEY, stringify);
    });
  }

  load() {
    const stringify = sessionStorage.getItem(PERSIST_KEY);
    const attendance = JSON.parse(stringify) || INITIAL;
    this.set(attendance);
  }

}
