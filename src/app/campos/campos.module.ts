import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { TextMaskModule } from 'angular2-text-mask';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';

import { RequisicoesCamposService } from './services/requisicoes-campos.service';

import { CampoDinamicoComponent } from './components/campo-dinamico/campo-dinamico.component';
import { TextoComponent } from './components/texto/texto.component';
import { ListaComponent } from './components/lista/lista.component';
import { CpfComponent } from './components/cpf/cpf.component';
import { CnpjComponent } from './components/cnpj/cnpj.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { DataComponent } from './components/data/data.component';
import { CpfCnpjComponent } from './components/cpf-cnpj/cpf-cnpj.component';
import { AnexoComponent } from './components/anexo/anexo.component';
import { InputDialogComponent } from './components/input-dialog/input-dialog.component';
import { DialogInputComponent } from './components/input-dialog/dialog-input/dialog-input.component';
import { ValorNumericoDirective } from '../directives/valor-numerico/valor-numerico.directive';
import { InteiroComponent } from './components/inteiro/inteiro.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { CpfCnpjDirective } from '../directives/cpf-cnpj/cpf-cnpj.directive';
import { ButtonDialogInputComponent } from './components/button-dialog-input/button-dialog-input.component';
import { DialogButtonComponent } from './components/button-dialog-input/dialog-button/dialog-button.component';
import { DialogTableComponent } from '../dialog-table/dialog-table.component';
import {MatPaginatorModule} from "@angular/material/paginator";

@NgModule({
  declarations: [
    CampoDinamicoComponent,
    TextoComponent,
    ListaComponent,
    CpfComponent,
    CnpjComponent,
    AutocompleteComponent,
    DataComponent,
    CpfCnpjComponent,
    AnexoComponent,
    InputDialogComponent,
    DialogInputComponent,
    ValorNumericoDirective,
    InteiroComponent,
    TextareaComponent,
    CpfCnpjDirective,
    DialogTableComponent,
    ButtonDialogInputComponent,
    DialogButtonComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MatStepperModule,
        MatInputModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule,
        MatCardModule,
        MatButtonModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        TextMaskModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatDialogModule,
        MatDividerModule,
        MatPaginatorModule
    ],
  providers: [
    RequisicoesCamposService,
  ],
  exports: [
    CampoDinamicoComponent,
    TextoComponent,
    ListaComponent,
    CpfComponent,
    CnpjComponent,
    AutocompleteComponent,
    DataComponent,
    CpfCnpjComponent,
    AnexoComponent
  ],
  entryComponents: [
    DialogInputComponent,
    DialogTableComponent,
    DialogButtonComponent
  ]
})
export class CamposModule { }
