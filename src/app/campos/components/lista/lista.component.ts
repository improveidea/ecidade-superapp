import { Component, Input, Output, EventEmitter, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ApiSelectService } from 'src/app/services/api.select.service';
import { RequisicaoCampo, RequisicoesCamposService } from '../../services/requisicoes-campos.service';

@Component({
  selector: 'app-campos-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit, AfterViewInit {

  @Input() campo: any;
  @Input() nomeSecao: string;
  @Input() value: any;
  @Input() formGroup: FormGroup;
  @Input() informacoesCampoDinamico$: Observable<any>;
  @Output() changedValue: EventEmitter<any>;

  public opcoes: any[] = [];
  public codigo = '';
  public selected: any;
  public urlCachedResponse = {};
  constructor(
    private readonly requisicoesCampos: RequisicoesCamposService,
    private readonly apiSelectService: ApiSelectService
  ) {
    this.changedValue = new EventEmitter<any>();
  }

  ngOnInit() {
    if (this.campo.bloquear != undefined && this.campo.bloquear) {
      this.formGroup.get(this.nomeSecao).get(this.campo.nome).disable();
    }

    this.codigo = this.value ? ( this.value.id || this.value.codigo ) :  '';
    if (this.campo.opcoes instanceof Array) {

      this.opcoes = this.campo.opcoes;

    } else if (!this.campo.opcoes) {

      this.opcoes = [];

    } else if (this.campo.opcoes && typeof this.campo.opcoes.endpoint === 'object') {

      if (this.campo.tipo === 'lista_dinamica') {

        const endpoint = this.campo.opcoes.endpoint;

        if ( endpoint.method.toUpperCase() === 'GET') {

          let path   = endpoint.path;
          const  params = [];

          if (endpoint.params !== null && endpoint.params.length > 0) {
            endpoint.params.forEach(param => {
              if (param.required === true && (param.value !== null || param.value !== '')) {
                params.push(param.name + '=' + param.value);
              }
            });
          }

          if (params.length > 0) {
            path += '?' + params.join('&');
          }

          if (this.urlCachedResponse[path] !== undefined) {
            this.opcoes =this.urlCachedResponse[path];
            return;
          }
          this.apiSelectService.api(endpoint.local).get(path).subscribe(result => {
            const options = this.apiSelectService.response(result);

            this.opcoes = options === undefined ? []: options;
            this.verifySelected();
            this.urlCachedResponse[path] = options;
            this.requisicoesCampos.setValue(this.campo.nome, 'opcoes', this.opcoes);
            this.requisicoesCampos.persist();
          });
        }
      }
    }
  }

  ngAfterViewInit() {
    this.informacoesCampoDinamico$.subscribe(result => {

      if (this.campo.nome !== result.nomeCampo) {
        return;
      }

      if (result.opcoes && result.opcoes instanceof Array) {

        if (result.opcoes.length > 0) {
          this.opcoes = result.opcoes;
        }
      }
    });
  }

  get campoControl() { return this.formGroup.get(this.nomeSecao).get(this.campo.nome); }

  hasErrors() {

    if (this.campoControl && this.campoControl.errors) {

      return true;
    }

    return false;
  }

  getError() {

    const errors = this.campoControl.errors;

    if (errors.required) {
      return 'required';
    }

    return '';
  }

  changeEvent(val) {
    this.opcoes.forEach(item => {
      if (item.id === val || item.codigo === val) {
        const res = {};
        res[this.campo.nome] = item;
        return this.changedValue.emit(res);
      }
    });
  }

  verifySelected() {
    const opcao = this.opcoes.find(opcaoFind => !!opcaoFind.selecionado);

    if (opcao) {
      this.codigo = (opcao.codigo || opcao.id);
    }
  }
}
