import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-campos-texto',
  templateUrl: './texto.component.html',
  styleUrls: ['./texto.component.scss']
})
export class TextoComponent implements OnInit {

  @Input() campo: any;
  @Input() nomeSecao: string;
  @Input() formGroup: FormGroup;
  @Output() changedValue: EventEmitter<any>;
  @Input() value: any;

  public maskObject: any = {
    mask: [],
    showMask: false
  };

  constructor() {
    this.changedValue = new EventEmitter<any>();
  }

  ngOnInit() {

    if (this.campo.mascara && this.campo.mascara != null) {

      this.maskObject = {
        mask: this.campo.mascara.split('').map(m => m.indexOf('0') !== -1 ? (new RegExp(/[0-9]/)).valueOf() : m),
        showMask: true
      };
    }

    if (this.campo.bloquear != undefined && this.campo.bloquear) {
      this.formGroup.get(this.nomeSecao).get(this.campo.nome).disable();
    }
  }

  get campoControl() { return this.formGroup.get(this.nomeSecao).get(this.campo.nome); }

  hasErrors() {

    if (this.campoControl && this.campoControl.errors) {

      return true;
    }

    return false;
  }

  isErrorRequired() {

    const errors = this.campoControl.errors;

    if (errors.required) {
      return true;
    }

    return false;
  }

  isErrorMin() {

    const errors = this.campoControl.errors;

    if (errors.min) {
      return true;
    }

    return false;
  }

  getError() {
    return this.campoControl.errors;
  }

  changeEvent(val) {
    val = this.ajustaValor(val);

    const res = {};
    res[this.campo.nome] = val;
    this.changedValue.emit(res);
  }

  private ajustaValor(val) {
    switch (this.campo.tipo.trim()) {
      case 'cpfCnpj':
      case 'cep':
        return val.replace(/\D/g, '').toString();
    }

    return val;
  }
}
