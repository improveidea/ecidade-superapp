import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AtributoService } from 'src/app/services/atributo.service';

@Component({
  selector: 'app-inteiro',
  templateUrl: './inteiro.component.html',
  styleUrls: ['./inteiro.component.scss']
})
export class InteiroComponent implements OnInit {

  @Input() campo: any;
  @Input() nomeSecao: string;
  @Input() formGroup: FormGroup;
  @Output() changedValue: EventEmitter<any>;
  @Input() value: any;

  private valorAnterior: number = 0;
  public maskObject: any = {
    mask: [],
    showMask: false
  };

  constructor(
    private readonly atributoService: AtributoService
  ) {
    this.changedValue = new EventEmitter<any>();
  }

  ngOnInit() {

    if (this.campo.mascara && this.campo.mascara != null) {

      this.maskObject = {
        mask: this.campo.mascara.split('').map(m => m.indexOf('0') !== -1 ? (new RegExp(/[0-9]/)).valueOf() : m),
        showMask: true
      };
    }

    if (this.campo.bloquear != undefined && this.campo.bloquear) {
      this.formGroup.get(this.nomeSecao).get(this.campo.nome).disable();
    }
  }

  get campoControl() { return this.formGroup.get(this.nomeSecao).get(this.campo.nome); }

  findCampoControl(campo) { return this.formGroup.get(this.nomeSecao).get(campo); }

  hasErrors() {

    if (this.campoControl && this.campoControl.errors) {

      return true;
    }

    return false;
  }

  isErrorRequired() {

    const errors = this.campoControl.errors;

    if (errors.required) {
      return true;
    }

    return false;
  }

  isErrorMin() {

    const errors = this.campoControl.errors;

    if (errors.min) {
      return true;
    }

    return false;
  }

  getError() {
    return this.campoControl.errors;
  }

  changeEvent(val) {
    const res = {};
    res[this.campo.nome] = val;
    this.changedValue.emit(res);
  }

  keyupEvent(event) {
    this.calculaPercentual();
    this.descobrePercentual();
    this.somaValores();
    this.limparCampos(event);
  }

  atualizaValorControl(val) {
    this.campoControl.setValue(val);
  }

  private calculaPercentual() {
    if (this.campo.calcPercentual) {
      const calcPercentual = this.campo.calcPercentual;
      const getPercent = this.findCampoControl(calcPercentual.getPercent);

      const precentual = (parseFloat(getPercent.value) || 0);

      if (precentual <= 100) {
        const getValor = this.findCampoControl(calcPercentual.getValor);
        const setValor = this.findCampoControl(calcPercentual.setValor);

        let valor: any = ((parseFloat(getValor.value) / 100) * parseFloat(getPercent.value));
        valor = (valor ? valor.toFixed(2) : 0);

        this.valorAnterior = parseFloat(getPercent.value);

        setValor.setValue(valor);

        return false;
      }

      getPercent.setValue(this.valorAnterior);
    }
  }

  private descobrePercentual() {
    if (this.campo.descobrePercentual) {
      const descobrePercentual = this.campo.descobrePercentual;
      const valorTotal = this.findCampoControl(descobrePercentual.getvalorTotal).value;
      const getValorCalculado = this.findCampoControl(descobrePercentual.getValorCalculado);
      const setValor = this.findCampoControl(descobrePercentual.setValor);

      const valorCalculado = (parseFloat(getValorCalculado.value) || 0);

      if (valorCalculado <= parseFloat(valorTotal)) {
        let valor: any = ((valorCalculado / parseFloat(valorTotal)) * 100);
        valor = (valor ? valor.toFixed(2) : 0);

        setValor.setValue(valor);

        return false;
      }

      getValorCalculado.setValue(valorTotal);
      this.descobrePercentual();
    }
  }

  somaValores() {
    if (this.campo.somarValores) {
      const somarValores = this.campo.somarValores;
      let valorTotal: any = 0;

      somarValores.getCamposValor.forEach(campo => {
        const campoControlAux = this.findCampoControl(campo);

        let valor: any = (campoControlAux.value || 0).toString();
        valor = valor.replace(/\./gi, ``).replace(',', '.');

        valorTotal += parseFloat(valor);
      });

      const campoControl = this.findCampoControl(somarValores.setValorCampo);

      valorTotal = valorTotal.toLocaleString('pt-BR', { minimumFractionDigits: 2 });

      campoControl.setValue(valorTotal);

      this.atributoService.sendMonetaryValue({
        field: somarValores.setValorCampo.trim(),
        value: valorTotal
      });
    }
  }

  private limparCampos(event) {
    if (Number(event.keyCode) === 9) {
      return;
    }

    if (this.campo.limparCampos) {
      const limparCampos = this.campo.limparCampos;

      limparCampos.forEach(campo => {
        this.findCampoControl(campo.nome).setValue(campo.valor);
      });
    }
  }
}
