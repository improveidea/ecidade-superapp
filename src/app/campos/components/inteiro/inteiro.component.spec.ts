import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InteiroComponent } from './inteiro.component';

describe('InteiroComponent', () => {
  let component: InteiroComponent;
  let fixture: ComponentFixture<InteiroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InteiroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InteiroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
