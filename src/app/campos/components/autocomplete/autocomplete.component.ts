import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {ApiSelectService} from 'src/app/services/api.select.service';

@Component({
    selector: 'app-campos-autocomplete',
    templateUrl: './autocomplete.component.html',
    styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {

    @Input() campo: any;
    @Input() nomeSecao: string;
    @Input() value: any;
    @Input() formGroup: FormGroup;
    @Input() informacoesCampoDinamico$: Observable<any>;
    @Output() changedValue: EventEmitter<any>;

    private opcoes: any[] = [];

    public opcoesFiltradas: Observable<any[]>;
    public maskObject: any = {
        mask: [],
        showMask: false
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private readonly apiSelectService: ApiSelectService
    ) {
        this.changedValue = new EventEmitter<any>();
    }

    async ngOnInit() {

        if (this.campo.mascara && this.campo.mascara != null) {

            this.maskObject = {
                mask: this.campo.mascara.split('').map(m => m.indexOf('0') !== -1 ? (new RegExp(/[0-9]/)).valueOf() : m),
                showMask: true
            };
        }

        this.opcoesFiltradas = this.campoControl.valueChanges
            .pipe(
                startWith(''),
                map(value => typeof value === 'string' || value == null ? value : value.descricao),
                map(value => this._filter(value))
            );

        if (this.campo.opcoes instanceof Array) {

            this.opcoes = this.campo.opcoes;

        } else if (!this.campo.opcoes) {

            this.opcoes = [];

        } else if (this.campo.opcoes && typeof this.campo.opcoes.endpoint === 'object') {

            let path = this.campo.opcoes.endpoint.path;
            const local = this.campo.opcoes.endpoint.local;
            const endpointParams = this.campo.opcoes.endpoint.params;
            const queryParams = [];

            if (endpointParams instanceof Array && endpointParams.length > 0) {
                endpointParams.forEach(param => {

                    switch (param.located) {

                        case 'query':
                            if (param.value) {
                                queryParams.push(param.name + '=' + param.value);
                            }
                            break;
                    }
                });
            }

            if (queryParams.length > 0) {
                path += '?' + queryParams.join('&');
            }

            const response = await this.apiSelectService.api(local).get(path).toPromise();
            const teste = this.apiSelectService.response(response, null, local);
            const itens = teste === undefined ? [] : teste;
            itens.forEach(item => {
                this.opcoes.push(item);
            });
        }

        this.informacoesCampoDinamico$.subscribe(result => {

            if (this.campo.nome !== result.nomeCampo) {
                return;
            }

            if (result.opcoes && result.opcoes instanceof Array) {

                if (result.opcoes.length > 0) {
                    this.opcoes = result.opcoes;
                }
            }
        });
        this.campoControl.setValue(this.value);
        this.cdr.detectChanges();
    }

    get campoControl() {
        return this.formGroup.get(this.nomeSecao).get(this.campo.nome);
    }

    hasErrors() {

        if (this.campoControl && this.campoControl.errors) {

            return true;
        }

        return false;
    }

    isErrorRequired() {

        const errors = this.campoControl.errors;

        if (errors.required) {
            return true;
        }

        return false;
    }

    isErrorMin() {

        const errors = this.campoControl.errors;

        if (errors.min) {
            return true;
        }

        return false;
    }

    isErrorObject() {
        const errors = this.campoControl.errors;

        if (errors.object) {
            return true;
        }

        return false;
    }

    getError() {
        return this.campoControl.errors;
    }

    changeEvent(val) {
        const res = {};
        res[this.campo.nome] = val;
        this.changedValue.emit(res);
    }

    closeEvent() {
        this.changeEvent(this.campoControl.value);
    }

    displayFn(obj?: any): string | undefined {
        return (obj && obj.descricao) ? obj.descricao : undefined;
    }

    private _filter(value: string): any[] {

        if (value == null) {
            return;
        }

        const filterValue = value.toLowerCase();

        return this.opcoes.filter(opcao => opcao.descricao.toLowerCase().includes(filterValue));
    }

}
