import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ViewContainerRef, AfterViewInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as textMask from 'vanilla-text-mask';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE
} from '@angular/material/core';
import {
    MomentDateAdapter,
    MAT_MOMENT_DATE_FORMATS,
    MAT_MOMENT_DATE_ADAPTER_OPTIONS
} from '@angular/material-moment-adapter';

@Component({
  selector: 'app-campos-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [
        MAT_DATE_LOCALE,
        MAT_MOMENT_DATE_ADAPTER_OPTIONS
      ]
    },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { strict: true }},
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class DataComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() campo: any;
  @Input() nomeSecao: string;
  @Input() value: any;
  @Input() formGroup: FormGroup;
  @Output() changedValue: EventEmitter<any>;

  @ViewChild('input', { read: ViewContainerRef, static: false }) public input;

  public minDate: any | null = null;
  public maxDate: any | null = null;

  public maskedInputController: any;

  constructor() {
    this.changedValue = new EventEmitter<any>();
  }

  ngOnInit() {
    if (this.value) {
      const parteData = this.value.split('/');
      this.value = new Date(`${parteData[1]}/${parteData[0]}/${parteData[2]}`);
    }
  }

  ngAfterViewInit() {
    this.maskedInputController = textMask.maskInput({
      inputElement: this.input.element.nativeElement,
      mask: [/[0-3]/, /[0-9]/, '\/', /[0-1]/, /[0-9]/, '\/', /\d/, /\d/, /\d/, /\d/]
    });
  }

  ngOnDestroy() {
    this.maskedInputController.destroy();
  }

  get campoControl() { return this.formGroup.get(this.nomeSecao).get(this.campo.nome); }

  hasErrors() {
    return (this.campoControl && this.campoControl.errors) ? true : false;
  }

  getError() {
    return this.campoControl.errors;
  }

  getErrorType() {

    const errors = this.campoControl.errors;

    if (errors.required) {
      return 'required';
    }

    if (errors.matDatepickerParse) {
      return 'matDatepickerParse';
    }

    return '';
  }

  changeEvent(val) {
    const res = {};
    res[this.campo.nome] = !!val ? val.format('L') : null;
    this.changedValue.emit(res);
  }
}
