import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-campos-cpf',
  templateUrl: './cpf.component.html',
  styleUrls: ['./cpf.component.scss']
})
export class CpfComponent implements OnInit {

  @Input() campo: any;
  @Input() nomeSecao: string;
  @Input() value: any;
  @Input() formGroup: FormGroup;
  @Output() changedValue: EventEmitter<any>;

  public maskObject: any = {
    mask: [],
    showMask: false
  };

  constructor(
    private readonly changeDetector: ChangeDetectorRef,
  ) {
    this.changedValue = new EventEmitter<any>();
  }

  ngOnInit() {

    if (this.campo.mascara && this.campo.mascara != null) {

      this.maskObject = {
        mask: this.campo.mascara.split('').map(m => m.indexOf('0') !== -1 ? (new RegExp(/[0-9]/)).valueOf() : m),
        showMask: true
      };
    }

    this.changeDetector.detectChanges();
  }

  get campoControl() { return this.formGroup.get(this.nomeSecao).get(this.campo.nome); }

  hasErrors() {

    if (this.campoControl && this.campoControl.errors) {

      return true;
    }

    return false;
  }

  getError() {

    const errors = this.campoControl.errors;

    if (errors.required) {
      return 'required';
    }

    if (errors.cpfInvalido) {

      return errors.cpfInvalido.reason;
    }
  }

  changeEvent(val) {
    this.changedValue.emit(val);
  }
}
