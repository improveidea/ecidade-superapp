import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextareaComponent implements OnInit {

  @Input() campo: any;
  @Input() nomeSecao: string;
  @Input() formGroup: FormGroup;
  @Output() changedValue: EventEmitter<any>;
  @Input() value: any;

  constructor() {
    this.changedValue = new EventEmitter<any>();
  }

  ngOnInit() {
    if (this.campo.bloquear != undefined && this.campo.bloquear) {
      this.formGroup.get(this.nomeSecao).get(this.campo.nome).disable();
    }
  }

  get campoControl() { return this.formGroup.get(this.nomeSecao).get(this.campo.nome); }

  hasErrors() {

    if (this.campoControl && this.campoControl.errors) {

      return true;
    }

    return false;
  }

  isErrorRequired() {

    const errors = this.campoControl.errors;

    if (errors.required) {
      return true;
    }

    return false;
  }

  isErrorMin() {

    const errors = this.campoControl.errors;

    if (errors.min) {
      return true;
    }

    return false;
  }

  getError() {
    return this.campoControl.errors;
  }

  changeEvent(val) {
    const res = {};
    res[this.campo.nome] = val;
    this.changedValue.emit(res);
  }

}
