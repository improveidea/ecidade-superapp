import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectorRef } from '@angular/core';
import {  FormGroup } from '@angular/forms';

@Component({
  selector: 'app-campos-cpf-cnpj',
  templateUrl: './cpf-cnpj.component.html',
  styleUrls: ['./cpf-cnpj.component.scss'],
})
export class CpfCnpjComponent implements OnInit {

  @Input() campo: any;
  @Input() nomeSecao: string;
  @Input() formGroup: FormGroup;
  @Output() changedValue: EventEmitter<any>;

  public maskObject: any = {
    mask: [],
    showMask: false
  };

  constructor(
      private readonly changeDetector: ChangeDetectorRef,
  ) {
    this.changedValue = new EventEmitter<any>();
  }

  ngOnInit() {

    console.log('montou cpf, cnpj')
    if (this.campo.mascara) {

      this.maskObject = {
        mask: this.campo.mascara.split('').map(m => m.indexOf('0') !== -1 ? (new RegExp(/[0-9]/)).valueOf() : m),
        showMask: true
      };
    }

    this.changeDetector.detectChanges();
  }

  get campoControl() { return this.formGroup.get(this.nomeSecao).get(this.campo.nome); }

  hasErrors() {

    if (this.campoControl && this.campoControl.errors) {
      const errors = this.campoControl.errors;
      if (errors.cpfInvalido) {
        if (errors.cpfInvalido.value === '___.___.___-__') {
          this.campoControl.setValue('');
        }
      }
      return true;
    }

    return false;
  }

  getError() {

    const errors = this.campoControl.errors;

    if (errors.required) {
      return 'required';
    }

    if (errors.cpfInvalido) {

      return errors.cpfInvalido.reason;
    }

    if (errors.cnpjInvalido) {

      return errors.cnpjInvalido.reason;
    }
  }

  changeEvent(val) {
    this.changedValue.emit(val);
  }
}
