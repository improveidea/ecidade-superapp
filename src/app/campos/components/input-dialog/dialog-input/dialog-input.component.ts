import { Component, ViewChild, Inject, OnInit} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApiSelectService } from 'src/app/services/api.select.service';
import { DialogInput } from 'src/app/models/dialog-input';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  templateUrl: './dialog-input.component.html',
  styleUrls: ['./dialog-input.component.scss']
})
export class DialogInputComponent implements OnInit {
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<object>;
  public dataTable: object[] = [];
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public pageSize = 5;

  public dadosCarregados = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    @Inject(MAT_DIALOG_DATA) public readonly data,
    private dialogRef: MatDialogRef<DialogInputComponent>,
    private readonly apiSelectService: ApiSelectService
  ) {}

  ngOnInit() {
    this.displayedColumns = this.data.opcoes.onOpen.colunas.map(coluna => coluna.nome);
    this.pageSize = (this.data.opcoes.onOpen.quantidade_inicial || 5);
    this.buscarDados();
  }

  filtrar(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  close() {
    this.dialogRef.close(null);
  }

  buscarDados() {
    const endpoint = this.data.opcoes.onOpen.endpoint;

    if (endpoint) {
      const dadosCampoSession = sessionStorage.getItem(this.data.nome);

      if (dadosCampoSession) {
        this.carregaValores(JSON.parse(dadosCampoSession));

        return false;
      }

      let path = endpoint.path;

      if (endpoint.params) {
        let params = endpoint.params.map((param) => `${param.name}=${param.value}`);

        params = (params.length > 0 ? `?${params.join('&')}` : '');

        path = path + params;
      }


      this.apiSelectService.api(endpoint.local).get(path).subscribe(response => {
        const dadosCampo = this.apiSelectService.response(response);

        sessionStorage.setItem(this.data.nome, JSON.stringify(dadosCampo));

        this.carregaValores(dadosCampo);
      });
    }
  }

  carregaValores(dados) {
    this.dadosCarregados = true;

    this.dataTable = dados;

    this.dataSource = new MatTableDataSource(this.dataTable);

    if (this.dataTable.length > 100) {
      this.pageSizeOptions.push(this.dataTable.length);
    }

    this.dataSource.paginator = this.paginator;
  }

  selecionar(row) {
    const colunaCodigo = this.data.opcoes.onOpen.colunas.find(coluna => !!coluna.codigo);
    const colunaDescricao = this.data.opcoes.onOpen.colunas.find(coluna => !!coluna.descricao);

    const dados: DialogInput = Object();

    if (colunaCodigo && colunaDescricao) {
      dados.row = row;
      dados.codigo = row[colunaCodigo.nome];
      dados.descricao = `${row[colunaCodigo.nome]} - ${row[colunaDescricao.nome]}`;
    }

    this.dialogRef.close(dados);
  }
}
