import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AtributoService } from 'src/app/services/atributo.service';
import { ApiSelectService } from './../../../services/api.select.service';
import { DialogInputComponent } from './dialog-input/dialog-input.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AttendanceService } from 'src/app/atendimento/services/attendance.service';

@Component({
  selector: 'app-input-dialog',
  templateUrl: './input-dialog.component.html',
  styleUrls: ['./input-dialog.component.scss']
})
export class InputDialogComponent implements OnInit {

  @Input() campo: any;
  @Input() nomeSecao: string;
  @Input() formGroup: FormGroup;

  public descricao: string = null;
  public buscandoDados: boolean = false;
  public dadosCarregados: boolean = false;
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<object>;
  public responseData: any = [];
  private camposReferencia: any[] = [];

  constructor(
    private readonly dialog: MatDialog,
    private readonly atributoService: AtributoService,
    private readonly apiSelectService: ApiSelectService,
    private readonly snackBar: MatSnackBar,
    private readonly attendance: AttendanceService,
  ) { }

  ngOnInit() {
    const onClose = this.campo.opcoes.onClose;

    this.displayedColumns = onClose.colunas.map(coluna => coluna.nome);

    if (this.campo.bloquear !== undefined && this.campo.bloquear) {
      this.formGroup.get(this.nomeSecao).get(this.campo.nome).disable();
    }

    this.atributoService.receiveMonetaryValue().subscribe(response => {
      for (const coluna of onClose.colunas) {
        if (coluna.hasOwnProperty('referencia')) {
          if (coluna.referencia.hasOwnProperty('functionBody')) {
            this.adicionaCampoReferencia(response);
            break;
          }
        }
      }
    });

    this.atributoService.receiveDialogClear().subscribe(() => {
      this.descricao = null;
      this.dadosCarregados = false;
      this.dataSource = new MatTableDataSource([]);
      this.responseData = [];
      this.camposReferencia = [];
    });
  }

  open() {
    if (this.campoControl.enabled) {
      const dialog = this.dialog.open(DialogInputComponent, {
        width: '90%',
        maxWidth: '80%',
        height: '80%',
        data: this.campo
      });

      dialog.afterClosed().subscribe(response => {
        if (response && Object.keys(response).length > 0) {
          this.campoControl.setValue(response.codigo);
          this.descricao = response.descricao;

          this.buscandoDados = true;
          this.dadosCarregados = false;

          const onClose = this.campo.opcoes.onClose;
          const endpoint = onClose.endpoint;

          let path = endpoint.path;

          if (endpoint.params) {
            const params = [];

            endpoint.params.forEach(param => {
              let valor = (param.valor || 0);

              if (param.referencia) {
                valor = this.formGroup.get((param.referencia.secao || this.nomeSecao)).get(param.referencia.campo).value;
              }

              params.push(`${param.nome}=${valor}`);
            });

            if (params.length > 0) {
              path += '?' + params.join('&');
            }
          }

          this.apiSelectService.api(endpoint.local).get(path).subscribe(response1 => {
            this.buscandoDados = false;
            this.dadosCarregados = true;
            this.responseData = this.apiSelectService.response(response1);
            this.dataSource = this.responseData;

            this.execFunctionOnclose();

            this.atributoService.receiveMonetaryValue().subscribe(response2 => {
              this.adicionaCampoReferencia(response2);
              this.execFunctionOnclose();
            });
          });
        }
      });
    }
  }

  get campoControl() { return this.formGroup.get(this.nomeSecao).get(this.campo.nome); }

  hasErrors() {
    if (this.campoControl && this.campoControl.errors) {
      return true;
    }

    return false;
  }

  isErrorRequired() {
    const errors = this.campoControl.errors;

    if (errors.required) {
      return true;
    }

    return false;
  }

  changeResponseValue(event: any, index: number, column: string) {
    this.responseData[index][column] = event.target.value;
  }

  private execFunctionOnclose() {
    const onClose = this.campo.opcoes.onClose;

    for (const coluna of onClose.colunas) {
      if (coluna.hasOwnProperty('referencia')) {
        if (coluna.referencia.hasOwnProperty('functionBody')) {
          let functionBody = coluna.referencia.functionBody;

          functionBody = ((functionBody instanceof Array) ? functionBody.join('') : functionBody);

          const fn = new Function('self, referencia, valor = 0', functionBody);

          if (coluna.referencia.hasOwnProperty('campo')) {
            if ((coluna.referencia.campo instanceof Object)) {
              const campo = this.camposReferencia.find(campoFind => campoFind.field === coluna.referencia.campo.nome);

              if (campo) {
                fn(this, coluna.referencia, campo.value);

                break;
              }
            } else {
              fn(this, coluna.referencia);

              break;
            }
          } else {
            fn(this, coluna.referencia);

            break;
          }
        }
      }
    }

    this.attendance.setValue(onClose.nome, this.responseData);
  }

  private removeMascaraMonetaria(valor) {
    return valor.replace(/\./gi, '').replace(',', '.');
  }

  execFunction(coluna) {
    const onClose = this.campo.opcoes.onClose;

    if (coluna.hasOwnProperty('referencia')) {
      if (coluna.referencia.hasOwnProperty('functionChange')) {
        let functionChange = coluna.referencia.functionChange;

        functionChange = ((functionChange instanceof Array) ? functionChange.join('') : functionChange);

        const fn = new Function('coluna, self', functionChange);

        fn(coluna, this);
      }
    }

    this.attendance.setValue(onClose.nome, this.responseData);
  }

  private adicionaCampoReferencia(dados) {
    const campo = this.camposReferencia.find(campoFind => campoFind.field === dados.field);

    if (!campo) {
      this.camposReferencia.push(dados);
    } else {
      // tslint:disable-next-line:forin
      for (const key in this.camposReferencia) {
        const campo2 = this.camposReferencia[key];

        if (campo2.field === dados.field) {
          this.camposReferencia.splice(Number(key), 1);

          this.camposReferencia.push(dados);

          break;
        }
      }
    }
  }

  private mensagem(mensagem: string) {
    this.snackBar.open(mensagem, 'X', {
      duration: 10000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
  }
}
