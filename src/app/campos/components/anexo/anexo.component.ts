import {Component, Input, OnInit, ChangeDetectorRef, ViewChild, AfterViewInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatChipList} from '@angular/material/chips';

import {CampoArquivo} from '../../../models/campo';
import {AttendanceService} from '../../../atendimento/services/attendance.service';
import {StateService} from '../../../services/state.service';
import {BehaviorSubject} from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-campos-anexo',
  templateUrl: './anexo.component.html',
  styleUrls: ['./anexo.component.scss']
})
export class AnexoComponent implements OnInit, AfterViewInit {

  @Input() campo: any;
  @Input() secao: any;
  @Input() nomeSecao: string;
  @Input() form: FormGroup;
  @Input() readonly: boolean;
  @Input() cleanedForm: BehaviorSubject<boolean>;
  @Input() nextStepClick: BehaviorSubject<boolean>;

  @ViewChild('listaArquivos', {static: false}) listaArquivos: MatChipList;

  public arquivos: CampoArquivo[];

  constructor(
    private readonly attendance: AttendanceService,
    private readonly state: StateService,
    private readonly changeDetector: ChangeDetectorRef,
  ) {
  }

  ngOnInit() {
    this.arquivos = [];

    this.cleanedForm.asObservable().subscribe(cleanedForm => {

      if (cleanedForm === false) {
        return;
      }

      this.arquivos = [];
      this.attendance.setValue('arquivos', this.arquivos);
    });
  }

  ngAfterViewInit() {

    this.nextStepClick.asObservable().subscribe(nextStepClicked => {
      if (nextStepClicked === false || nextStepClicked == null) {
        return;
      }

      if (this.hasErrors()) {

        if (this.isErrorRequired()) {

          this.campoControl.markAsTouched();
          this.listaArquivos.errorState = true;
        }
      }
    });
  }

  get campoControl() {
    return this.form.get(this.nomeSecao).get(this.campo.nome);
  }

  hasErrors() {

    if (this.campoControl && this.campoControl.errors) {

      return true;
    }

    return false;
  }

  isErrorRequired() {

    const errors = this.campoControl.errors;

    if (errors.required) {
      return true;
    }

    return false;
  }

  enabled(): boolean {
    return !this.readonly;
  }

  removerArquivo(file: any) {

    const indice = this.arquivos.indexOf(file);

    if (indice >= 0) {
      this.arquivos.splice(indice, 1);
      this.attendance.removeValue('arquivos', file);
      this.attendance.persist();
    }

    this.listaArquivos.errorState = false;

    if (this.arquivos.length === 0) {

      this.campoControl.reset();

      if (this.isErrorRequired()) {
        this.campoControl.markAsTouched();
        this.listaArquivos.errorState = true;
      }
    }
  }

  adicionarArquivo(file: any) {

    const nomeAnexo = this.campo.nome;

    const getContent = (data: File) => {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        console.log(FileReader);
        if (data.type === '') {
          reject({message: 'Arquivo sem extensão'});
        }
        reader.onload = () => {
          resolve({
            id: null,
            descricao: data.name,
            nome: nomeAnexo,
            conteudo: reader.result,
            type: data.type,
          });
        };
         reader.onerror =  (err) => reject(err);
        reader.readAsDataURL(data);
      });
    };

    Promise.all(Array.from(file.files).map((data: File) => getContent(data)))
      .then(function(files: CampoArquivo[]) {

        files.forEach(function(fileItem) {
          this.arquivos.push(fileItem);
          this.attendance.addValue('arquivos', fileItem);
        }.bind(this));

        this.attendance.persist();
        this.changeDetector.detectChanges();

        this.arquivos.forEach(arquivo => {

          if (arquivo.nome === this.campo.nome) {
            this.listaArquivos.errorState = false;
            this.campoControl.setValue(arquivo.descricao);
          }
        });
      }.bind(this))
      .catch((err: any) => {
        if (err.message) {
          Swal.fire('', err.message, 'error');
        }
        console.error('error - adicionarArquivo', err);
      });
  }

  dispatchClick(element: HTMLInputElement) {
    element.click();
  }
}
