import { Component, Input, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

import { RequisicoesCamposService } from '../../services/requisicoes-campos.service';
import { cpfInvalidoValidator as CpfInvalidoValidator } from '../../../directives/cpf-invalido.directive';
import { cnpjInvalidoValidator as CnpjInvalidoValidator } from '../../../directives/cnpj-invalido.directive';
import { ApiSelectService } from 'src/app/services/api.select.service';
import { AtributoService } from 'src/app/services/atributo.service';
import { DialogTableComponent } from 'src/app/dialog-table/dialog-table.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-campo-dinamico',
  templateUrl: './campo-dinamico.component.html',
  styleUrls: ['./campo-dinamico.component.scss']
})
export class CampoDinamicoComponent implements OnInit, AfterViewInit {

  @Input() form: FormGroup;
  @Input() secao: any;
  @Input() campo: any;
  @Output() customChange: EventEmitter<any> = new EventEmitter();

  public opcoes: any[] = [];
  public informacoesCompartilhadas: BehaviorSubject<any> = new BehaviorSubject<any>({});
  private identificador = 'campo-dinamico';
  value: any;
  constructor(
      private requisicoesCampos: RequisicoesCamposService,
      private readonly apiSelectService: ApiSelectService,
      private readonly atributoService: AtributoService,
      private readonly dialog: MatDialog,
      private readonly snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {

    this.requisicoesCampos.setField(this.campo.nome, {
      campo: this.campo,
      opcoes: [],
    });

    this.requisicoesCampos.persist();

    if (this.campo.validacoes !== undefined && this.campo.validacoes instanceof Array) {

      if (this.campo.validacoes.length > 0) {

        this.campo.validacoes.forEach(validacao => {

          switch (validacao.tipo) {

            case 'valor':

              if (this.isCampoTexto() && ['date', 'data'].find(tipo => this.campo.tipo === tipo) === undefined) {

                // eslint-disable-next-line no-underscore-dangle
                const validators = this._addValidatorField(this.campo);

                if (validacao.minimo !== undefined) {
                  validators.push(Validators.min(validacao.minimo));
                }

                this.form.get(this.secao.nome).get(this.campo.nome).setValidators(validators);
                this.form.get(this.secao.nome).get(this.campo.nome).updateValueAndValidity();
                this.form.get(this.secao.nome).get(this.campo.nome).setValue(this.value ? this.value : '' );
              }
              break;
          }
        });
      }
    }

    this.apiSelectService.setRefErro(this.identificador);

    this.apiSelectService.receiveErro().subscribe((response) => {
      if (response.identificador === this.identificador) {
        this.message(response.mensagem);
      }
    });
  }

  ngAfterViewInit() {
    this.requisicoesCampos.get().subscribe(requisicoes => {

      const requisicaoCampo = requisicoes.map.get(this.campo.nome);

      if (requisicoes.nomeCampo !== this.campo.nome) {
        return;
      }

      this.opcoes = requisicaoCampo.opcoes;

      this.informacoesCompartilhadas.next({
        nomeCampo: this.campo.nome,
        opcoes: this.opcoes
      });
    });
  }

  changeChild(res: any) {
    if (this.secaoControl().get(this.campo.nome).errors) {
      return;
    }

    this.atributoService.sendFieldValueChanged({
      section: this.secao.nome,
      field: this.campo.nome
    });

    if (this.campo.hasOwnProperty('on')) {
      this.customChange.emit({
        campo: this.campo,
        secao: this.secao,
        eventos: this.campo.on
      });
    }

    if (typeof this.campo.change === 'object') {

      const change = this.campo.change;

      let qtdValorInformados = 0;

      if (typeof change.endpoint === 'object') {

        let path = change.endpoint.path;
        const queryParams = [];

        change.endpoint.params.forEach(param => {

          let paramValue = this.getValorCampo(res);
          let pathParamName = '{';

          if (param.required && param.value && paramValue === null) {
            paramValue = param.value;
          }

          if (paramValue) {
            qtdValorInformados++;
          }

          switch (param.located) {

            case 'query':
              queryParams.push(param.name + '=' + paramValue);
              break;

            case 'path':
              pathParamName += param.name;
              pathParamName += '}';
              path = path.replace(pathParamName, paramValue);
              break;
          }
        });

        if (qtdValorInformados === 0) {
          this.limparCamposPreencher();
        } else {
          if (queryParams.length > 0) {
            path += '?' + queryParams.join('&');
          }

          if (change.showLoading) {
            this.atributoService.sendShowLoading(true);
          }

          this.apiSelectService.api(change.endpoint.local).get(path).subscribe(result => {
            if (change.showLoading) {
              this.atributoService.sendShowLoading(false);
            }

            // @ts-ignore
            let responseObj: any;

            // eslint-disable-next-line prefer-const
            responseObj = this.apiSelectService.response(result);

            if (change.hasOwnProperty('dialog')) {
              if (responseObj.length >= change.dialog.from) {
                this.openDialog(responseObj);
              } else {
                this.camposPreencher(this.validateResponse(responseObj, change.endpoint.local));
              }
            } else {
              this.camposPreencher(this.validateResponse(responseObj, change.endpoint.local));
            }
          }, (error) => {
            this.apiSelectService.responseErro(error);

            if (change.showLoading) {
              this.atributoService.sendShowLoading(false);
            }

            if (change.hasOwnProperty('fnErrorChange')) {
              const fnErrorChange = change.fnErrorChange.join('');

              const fn = new Function('error, self', fnErrorChange);

              fn(error, this);
            }
          });
        }
      }

      if (change.validacoes && change.validacoes instanceof Array) {

        change.validacoes.forEach(validacao => {

          switch (validacao.tipo) {
            case 'requerido_condicional':

              const secao = this.form.get(this.secao.nome);
              let valorCampoAtual = res;

              switch (this.campo.tipo) {
                case 'lista':
                case 'lista_dinamica':
                case 'autocomplete':
                  valorCampoAtual = res[this.campo.nome];

                  if (valorCampoAtual.codigo) {
                    valorCampoAtual = valorCampoAtual.valorCampoAtual.codigo;
                  }

                  if (valorCampoAtual.id) {
                    valorCampoAtual = valorCampoAtual.id;
                  }

                  break;
              }

              validacao.campos.forEach(campoValidar => {

                let validar = false;

                if (validacao.comparacao === '=') {
                  validar = !!validacao.valor.find(valorAceito => valorAceito === valorCampoAtual);
                }

                const campoValidarInstancia = this.secao.campos.find(c => c.nome === campoValidar.nome);
                const validators = this._addValidatorField(campoValidarInstancia);

                if (!validar) {
                  secao.get(campoValidar.nome).reset();
                  secao.get(campoValidar.nome).setValidators(validators);
                  secao.get(campoValidar.nome).updateValueAndValidity();
                  return;
                }

                validators.push(Validators.required);

                secao.get(campoValidar.nome).setValidators(validators);
                secao.get(campoValidar.nome).updateValueAndValidity();
              });
              break;
          }
        });
      }
    }

    if (res[this.campo.nome].hasOwnProperty('bloquear') && res[this.campo.nome].bloquear instanceof Array) {
      res[this.campo.nome].bloquear.forEach(campo => {
        const campoControl = this.secaoControl(campo.secao).get(campo.nome);

        campoControl.disable();

        if (campo.hasOwnProperty('limpar') && campo.limpar) {
          campoControl.setValue(null);
        }
      });
    } else {
      if (res[this.campo.nome].hasOwnProperty('desbloquear') && res[this.campo.nome].desbloquear instanceof Array) {
        res[this.campo.nome].desbloquear.forEach(campo => {
          const campoControl = this.secaoControl(campo.secao).get(campo.nome);

          campoControl.enable();

          if (campo.hasOwnProperty('limpar') && campo.limpar) {
            campoControl.setValue(null);
          }
        });
      }
    }
  }

  private validateResponse(responseObj, local = null) {
    return ((responseObj instanceof Array) && local !== 'ecidade') ? responseObj[0] : responseObj;
  }

  secaoControl(secao = null) {
    return this.form.get(secao || this.secao.nome);
  }

  private limparCamposPreencher() {
    const change = this.campo.change;

    change.campos_preencher.forEach(c => {

      let secao = this.form.get(this.secao.nome);

      if (c.secao) {
        secao = this.form.get(c.secao);
      }

      const field = secao.get(c.campo);

      if (field) {
        if (c.valor) {
          field.setValue('');
        }

        if (c.desbloquear) {
          field.disable();
        }

        if (c.bloquear) {
          field.enable();
        }
      }
    });
  }

  private camposPreencher(responseObj: any) {
    const change = this.campo.change;

    change.campos_preencher.forEach(c => {

      let secao = this.form.get(this.secao.nome);

      if (c.secao) {
        secao = this.form.get(c.secao);
      }

      const field = secao.get(c.campo);

      if (field) {

        if (c.valor) {
          const value = this._getNestedAtributeFromObject(c.valor, responseObj, (c.geraObjeto || null));
          field.setValue(value);
        }

        if (c.desbloquear) {
          field.enable();
        }

        if (c.bloquear) {
          field.disable();
        }

        if (c.opcoes) {

          let items = [];
          const opcoes = [];

          if (responseObj instanceof Array) {
            items = responseObj;
          } else {
            items.push(responseObj);
          }

          items.forEach(item => {

            opcoes.push(
                {
                  id: this._getNestedAtributeFromObject(c.opcoes.id, item),
                  descricao: this._getNestedAtributeFromObject(c.opcoes.descricao, item)
                });
          });

          this.requisicoesCampos.setValue(c.campo, 'opcoes', opcoes);
        }
      }
    });

    this.requisicoesCampos.persist();
  }

  openDialog(responseParam) {
    const dialog = this.dialog.open(DialogTableComponent, {
      width: '90%',
      maxWidth: '80%',
      height: '80%',
      data: {
        field: this.campo,
        response: responseParam
      }
    });

    dialog.afterClosed().subscribe(response => {
      if (response) {
        this.camposPreencher(response);
      } else {
        this.resetFormSecao();
      }
    });
  }

  private resetFormSecao() {
    this.form.get(this.secao.nome).reset();
  }

  private _addValidatorField(campo: any): any[] {

    const validators = [];

    if (campo.tipo === 'cpf') {
      validators.push(CpfInvalidoValidator());
    }

    if (campo.tipo === 'cnpj') {
      validators.push(CnpjInvalidoValidator());
    }

    if (campo.obrigatorio) {
      validators.push(Validators.required);
    }

    return validators;
  }

  private _getNestedAtributeFromObject(item: string, object: object, geraObjeto = null): any {

    let a = item.split('.');

    if (a.length > 1) {

      a = a.reduce((obj, val) => {
        if (obj !== null && val !== null) {
          return obj[val];
        }
        return null;
      }, object);

    } else {
      if (geraObjeto) {
        return {
          id: object[geraObjeto.id],
          descricao: object[geraObjeto.descricao]
        };
      }

      a = object[item];
    }

    return a;
  }

  isCampoTexto(): boolean {

    switch (this.campo.tipo) {
      case 'texto':
      case 'string':
      case 'date':
      case 'data':
      case 'cpf':
      case 'cnpj':
      case 'cpfCnpj':
      case 'email':
      case 'inteiro':
      case 'cep':
        return true;
    }

    return false;
  }

  getValorCampo(obj): any {

    const valorCampo = obj ? obj[this.campo.nome] : null;

    if (this.isCampoTexto()) {
      return valorCampo;
    }

    switch (this.campo.tipo) {
      case 'lista':
      case 'lista_dinamica':
      case 'autocomplete':
        if (valorCampo && valorCampo.id) {
          return valorCampo.id;
        } else if (valorCampo && valorCampo.codigo) {
          return valorCampo.codigo;
        }
        break;
    }

    return null;
  }

  private message(message: string) {
    this.snackBar.open(message, 'X', {
      duration: 10000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
  }
}
