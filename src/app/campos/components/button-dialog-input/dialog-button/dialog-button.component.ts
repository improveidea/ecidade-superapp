import {Component, Inject, OnInit} from '@angular/core';
import {RequisicoesCamposService} from 'src/app/campos/services/requisicoes-campos.service';
import {ApiSelectService} from 'src/app/services/api.select.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  templateUrl: './dialog-button.component.html',
  styleUrls: ['./dialog-button.component.scss']
})
export class DialogButtonComponent implements OnInit {

  public title: string;
  public inputs: any = [];
  public loaded: boolean;
  public type: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public readonly data,
    private dialogRef: MatDialogRef<DialogButtonComponent>,
    private readonly apiSelectService: ApiSelectService,
    private readonly requisicoesCampos: RequisicoesCamposService,
  ) {
  }

  ngOnInit() {
    this.title = this.data.label;
    const onOpen = this.data.onOpen;

    this.type = (onOpen.hasOwnProperty('input') && onOpen.input.type) ? onOpen.input.type : 'texto';

    if (onOpen.endpoint) {
      const dadosCampoSession = this.requisicoesCampos.getValue(this.data.nome, 'opcoes');

      if (dadosCampoSession.length > 0) {
        this.carregaDados(dadosCampoSession);

        return false;
      }

      let path = onOpen.endpoint.path;

      if (onOpen.endpoint.params) {
        let params = onOpen.endpoint.params.map((param) => `${param.name}=${param.value}`);

        params = (params.length > 0 ? `?${params.join('&')}` : '');

        path = path + params;
      }

      this.apiSelectService.api(onOpen.endpoint.local).get(path).subscribe(response => {
        const dadosCampo = this.apiSelectService.response(response);

        this.carregaDados(dadosCampo);

        this.persist();
      });
    }
  }

  private carregaDados(dados) {
    this.loaded = true;

    this.inputs = dados;
  }

  close() {
    this.dialogRef.close(null);
  }

  save() {
    this.persist();

    this.close();
  }

  private persist() {
    this.requisicoesCampos.setValue(this.data.nome, 'opcoes', this.inputs);
    this.requisicoesCampos.persist();
  }

  add(event, row) {
    // tslint:disable-next-line:forin
    for (const key in this.inputs) {
      const element = this.inputs[key];

      if (element.codigo === row.codigo) {
        this.inputs[key].value = event.target.value.trim();

        break;
      }
    }
  }
}
