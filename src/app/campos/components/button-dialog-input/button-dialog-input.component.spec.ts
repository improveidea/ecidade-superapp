import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonDialogInputComponent } from './button-dialog-input.component';

describe('ButtonDialogInputComponent', () => {
  let component: ButtonDialogInputComponent;
  let fixture: ComponentFixture<ButtonDialogInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonDialogInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonDialogInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
