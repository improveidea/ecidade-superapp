import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DialogButtonComponent } from './dialog-button/dialog-button.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-button-dialog-input',
  templateUrl: './button-dialog-input.component.html',
  styleUrls: ['./button-dialog-input.component.scss']
})
export class ButtonDialogInputComponent implements OnInit {

  @Input() campo: any;
  @Input() nomeSecao: string;
  @Input() formGroup: FormGroup;

  constructor(
    private readonly dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  open() {
    this.dialog.open(DialogButtonComponent, {
      width: '90%',
      maxWidth: '80%',
      height: '80%',
      data: this.campo
    });
  }
}
