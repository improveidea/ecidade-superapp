import { TestBed } from '@angular/core/testing';

import { RequisicoesCamposService } from './requisicoes-campos.service';

describe('RequisicoesCamposService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequisicoesCamposService = TestBed.get(RequisicoesCamposService);
    expect(service).toBeTruthy();
  });
});
