import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as _ from 'lodash';

export interface RequisicaoCampo {
  campo: any;
  opcoes: any[];
}

export interface Requisicoes {
  nomeCampo: string;
  map: Map<string, RequisicaoCampo>;
}

const REQUISICOES_INICIAIS: Requisicoes = {
    nomeCampo: '',
    map: new Map()
};

const REQUISICOES_PERSIST_KEY = 'requisicoes_campos_dinamicos';

const  EMPTY_REQUISICAO = {
    campo: {},
    opcoes: []
};
export {REQUISICOES_INICIAIS, REQUISICOES_PERSIST_KEY, EMPTY_REQUISICAO};
@Injectable({
  providedIn: 'root'
})
export class RequisicoesCamposService {

  private readonly requisicoes  = new BehaviorSubject<any>(REQUISICOES_INICIAIS);
  private readonly requisicoes$ = this.requisicoes.asObservable();

  constructor() { }

  get() {
    return this.requisicoes$;
  }

  set(requisicoes: Requisicoes) {
    Promise.resolve().then(() => {
      this.requisicoes.next(requisicoes);
    });
  }

  setField(field: string, requisicao: RequisicaoCampo) {
    const reqs = this.requisicoes.getValue();
    reqs.nomeCampo = field;
    reqs.map.set(field, requisicao);

    this.set(reqs);
  }

  removeField(field: string) {
    const reqs = this.requisicoes.getValue();
    reqs.map.delete(field);

    this.set(reqs);
  }

  getValue(field: string, key: keyof RequisicaoCampo) {
    const reqs = this.requisicoes.getValue();
    const req  = reqs.map.get(field);

    return req && req[key] ? req[key] : null;
  }

  setValue(field: string, key: keyof RequisicaoCampo, value: any) {
    const reqs = this.requisicoes.getValue();
    let req  = reqs.map.get(field);

    if (!req) {
      req = EMPTY_REQUISICAO;
    }

    req[key] = value;
    reqs.nomeCampo = field;
    reqs.map.set(field, req);

    this.set(reqs);
  }

  addValue(field: string, key: keyof RequisicaoCampo, value: any) {
    const reqs = this.requisicoes.getValue();
    let req  = reqs.map.get(field);

    if (!req) {
      req = EMPTY_REQUISICAO;
    }

    if (req[key] instanceof Array) {

      req[key].push(value);
      reqs.nomeCampo = field;
      reqs.map.set(field, req);

      this.set(reqs);
    }
  }

  removeValue(field: string, key: keyof RequisicaoCampo, value: any) {
    const reqs = this.requisicoes.getValue();
    const req  = reqs.map.get(field);

    if (!req) {
      return;
    }

    if (req[key] instanceof Array) {

      const keyValues: any = [];
      req[key].forEach(item => {

        if (!(_.isEqual(item, value))) {
          keyValues.push(item);
        }
      });

      req[key] = keyValues;
      reqs.map.set(field, req);

      this.set(reqs);
    }
  }

  clear() {
    this.set(REQUISICOES_INICIAIS);
  }

  persist() {
    Promise.resolve().then(() => {
      const itens = [];
      const  reqs = this.requisicoes.getValue();
      const  map  = reqs.map;
      const  reqsIterator = map.keys();
      for (let i = 0; i < map.size; i++) {
        const key = reqsIterator.next().value;
        const obj = map.get(key);
        itens.push({
           key,
           obj
        });
      }

      sessionStorage.setItem(REQUISICOES_PERSIST_KEY, JSON.stringify(itens));
    });
  }

  load() {
    const stringify = sessionStorage.getItem(REQUISICOES_PERSIST_KEY);
    const  reqs = JSON.parse(stringify) || REQUISICOES_INICIAIS;
    this.set(reqs);
  }
}
