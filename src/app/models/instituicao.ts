export class Instituicao {
  public descricao: string;
  public descricaoAbreviada: string;
  public cnpj: string;
  public email: string;
  public logradouro: string;
  public municipio: string;
  public bairro: string;
  public telefone: string;
  public site: string;
  public numero: string;
  public complemento: string;
  public uf: string;
  public cep: string;
  public fax: string;
}
