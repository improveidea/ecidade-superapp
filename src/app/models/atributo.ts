export class Unlock {
  nome: string;
}

export class Block {
  nome: string;
}

export class Monetary {
  field: string;
  value: string;
}

export class ValueChanged {
  section: string;
  field: string;
}
