export class EcidadeAccessToken {
  // tslint:disable-next-line:variable-name
  public token_type: string;
  // tslint:disable-next-line:variable-name
  public expires_in: number;
  // tslint:disable-next-line:variable-name
  public access_token: string;
}
