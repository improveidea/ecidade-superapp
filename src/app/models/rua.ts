export class Rua {
  id: number;
  prefixo: string;
  prefixoAbreviado: string;
  descricao: string;
}
