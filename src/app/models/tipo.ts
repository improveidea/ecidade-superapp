export class Tipo {
  public id: number;
  public descricao: string;
  public rota: string;
  public identificado: string;
}

export class RespostaArquivo {

  clients: [];
  createdAt: string;
  id: number;
  mimeType: string;
  name: string;
  ownerId: number;
  size: number;
  updatedAt: string;
  url: string;
  visibility: string;
}
