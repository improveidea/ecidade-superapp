export class Campo {
  id: number;
  descricao: string;
  obrigatorio: boolean;
  tipo: number;
}

export class CampoArquivo {
  descricao: string;
  id: number;
  nome: string;
  conteudo: any;
  type: string;
}

export interface CampoFormulario {
  nome: string;
  label: string;
  tipo: string;
  tamanho: number;
  obrigatorio: boolean;
}
