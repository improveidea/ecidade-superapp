import { Tipo } from './tipo';

export class GrupoTipo {
  public id: number;
  public nome: string;
  public descricao: string;
  public tipos: Tipo[];
}
