import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private apiService: ApiService, private router: Router, private authService: AuthService) {
  }

  async setCurrentCity(city) {
    const data = {
      city: city.id,
    };
    const response = await this.apiService.post('me/change-city', data).toPromise().catch((error) => ({
      error: true,
      message: error.error.message
    }));
    return response.data;
  }

  async getUserData(redirectIfUserNull = true) {
    const response = await this.apiService.get('me').toPromise().catch((error) => ({
      error: true,
      message: error.error.message,
      code: error.status_code
    }));
    if (response.error) {
      return;
    }
    await localStorage.setItem('user_data', JSON.stringify(response.data));
    await this.authService.notifyUserData(response.data);

    return response;
  }

  getDocument() {
    const data = JSON.parse(localStorage.getItem('user_data'));
    return data?.document || '';
  }

  async cancelAccount() {
    const response = await this.apiService.delete('users/me/account').toPromise().catch((error) => ({
      error: true,
      message: error.error.message,
      code: error.status_code
    }));
    if (response.error) {
      return false;
    }

    return true;
  }


}
