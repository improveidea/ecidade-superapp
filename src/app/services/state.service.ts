import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

/**
 * Interface de definição do stado da aplicação
 */
export interface State {
    identificado: boolean;
    user?: any;
    tipo: any;
    numeroAtendimento: number;
}

export const INITIAL_STATE: State = {
    identificado: null,
    user: null,
    tipo: null,
    numeroAtendimento: null,
};

export const PERSIST_KEY_STATE = 'state';

@Injectable({
    providedIn: 'root'
})
export class StateService {

    private readonly state = new BehaviorSubject<State>({...INITIAL_STATE});
    public readonly state$ = this.state.asObservable();

    constructor() {
    }

    get() {
        return this.state$;
    }

    set(state: State) {
        Promise.resolve().then(() => {
            this.state.next({...state});
        });
    }

    setValue(key: keyof State, value: any) {
        const state = this.state.getValue();
        // @ts-ignore
        state[key] = value;
        const stringify = JSON.stringify(state);
        sessionStorage.setItem(PERSIST_KEY_STATE, stringify);
        this.set(state);
    }

    getValue(key = null) {
        const stringify = sessionStorage.getItem(PERSIST_KEY_STATE);
        const state = JSON.parse(stringify) || {};
        if (key === null) {
            return state;
        }
        return state[key] || null;
    }

    getValueFromSession() {
        return JSON.parse(sessionStorage.state);
    }

    clear() {
        this.set(INITIAL_STATE);
    }

    persist() {
        Promise.resolve().then(() => {
            const state = this.state.getValue();
            const stringify = JSON.stringify(state);
            sessionStorage.setItem(PERSIST_KEY_STATE, stringify);
        });
    }

    load() {
        const stringify = sessionStorage.getItem(PERSIST_KEY_STATE);
        const state = JSON.parse(stringify) || INITIAL_STATE;
        this.set(state);
    }
}
