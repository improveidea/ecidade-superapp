import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpParams} from '@angular/common/http';
import {ApiService} from './api/api.service';

@Injectable({
  providedIn: 'root'
})
export class TiposService {

  private tipoPrimeiroAcesso$: Observable<any[]> = null;

  constructor(
    private api: ApiService,
  ) { }

  getMenu(): Observable<any[]> {

    const params = new HttpParams();
    const options = {
      params: params.append('formareclamacao', environment.formaReclamacao ? environment.formaReclamacao.toString() : ''),
      responseType: 'json'
    };
    return this.api.get('solicitacoes/menu', options);
  }

  getTipoPrimeiroAcesso() {
    const params = new HttpParams();
    const options = {
      params: params.append('formareclamacao', '9'),
      responseType: 'json'
    };

    if (this.tipoPrimeiroAcesso$ === null) {
      this.tipoPrimeiroAcesso$ = this.api.get('solicitacoes/menu', options);
    }
    return this.tipoPrimeiroAcesso$;
  }
}
