import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {ApiService} from './api/api.service';
import {EcidadeService} from './ecidade.service';
import {FormsDataService} from '../solicitacao-atendimento/services/forms-data.service';

@Injectable({
  providedIn: 'root'
})
export class ApiSelectService {

  private local: string;
  private refErro: string;
  private erro = new Subject();

  constructor(
    private readonly apiService: FormsDataService,
    private readonly ecidadeService: EcidadeService
  ) {}

  setRefErro(refErro: string) {
    this.refErro = refErro;
  }

  api(local) {
    this.local = local;

    switch (local) {
      case 'ecidade':
        return this.ecidadeService;
      default:
        return this.apiService;
    }
  }

  response(response: any, callback: any = null, local = null) {
    let data = response.data  || response;
    if (callback) {
      return callback(data);
    }

    return data;
  }

  errorCallback(response: any, callback: any = null) {
    let error: any = response.message;

    switch (this.local) {
      case 'ecidade':
        error = response.error.message;
        break;
    }

    callback(error);
  }

  responseErro(response: any) {
    switch (this.local) {
      case 'ecidade':
        if (response.error) {
          this.message(response.error.message);
        }
        break;
    }
  }

  private message(message: string) {
    if (this.refErro) {
      this.erro.next({
        identificador: this.refErro,
        mensagem: message
      });
    }
  }

  public receiveErro(): Observable<any> {
    return this.erro.asObservable();
  }
}
