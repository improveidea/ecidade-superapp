import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import {ApiService} from './api/api.service';

@Injectable({
  providedIn: 'root'
})
export class EcidadeService {

  constructor(private readonly apiService: ApiService,) {}

  public get(path: string): Observable<any> {
    const endpoint = 'forms/data?type=ecidade&url='+path;
    return this.apiService.get(endpoint);
  }
  public post(endpoint: string, data: any, formData = false): Observable<any> {

    const url = 'forms/data?type=ecidade&url='+endpoint;
    return this.apiService.post(`${url}`, data, formData);
  }

  public getPrefix() {
    return '';
  }

  public makeUrl(suffix: string) {
    return this.getPrefix() + suffix;
  }
}
