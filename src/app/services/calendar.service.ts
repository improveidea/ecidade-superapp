import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CalendarService {

    getMonthName(month) {
        const months = [
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro",
        ];
        return months[(month - 1)];
    }

}
