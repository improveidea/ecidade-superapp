import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoaderService {

    activeSubject$ = new BehaviorSubject<any>(false);

    constructor(private apiService: ApiService) {
    }

    setActive(active: boolean) {
        this.activeSubject$.next(active);
    }
}
