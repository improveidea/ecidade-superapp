import {Injectable} from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {


    constructor(private authService: AuthService){
    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = this.authService.getAccessToken();
        if (token) {
            request = request.clone({headers: request.headers.set('Authorization', 'Bearer ' + token)});
        }
        if (!request.headers.has('Content-Type')) {
            request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
        }

        if (request.headers.has('Content-Type') && request.headers.get('Content-Type') === 'x') {
            request = request.clone({headers: request.headers.delete('Content-Type')});
        }
        request = request.clone({headers: request.headers.set('Accept', 'application/json')});


        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                    if (error.error.error === 'invalid_token') {

                    } else {
                    }
                }
                return throwError(error);
            }));
    }

}

