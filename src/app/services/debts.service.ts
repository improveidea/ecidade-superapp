import {Injectable} from '@angular/core';
import {ApiService} from "./api/api.service";
import {ToastController} from "@ionic/angular";
import {Clipboard} from "@awesome-cordova-plugins/clipboard/ngx";

@Injectable({
    providedIn: 'root'
})
export class DebtsService {

    constructor(private api: ApiService, private toast: ToastController, private clipboard: Clipboard) {
    }

    public async copyLine(debt) {

        if (debt.status === 'registered' || !debt.need_registry) {
            await this.clipboard.copy(debt.digitable_line);
            const toast = await this.toast.create({
                message: "Linha digitável copiada. O débito já encontra-se disponível para pagamento.",
                duration: 3000,
            });
            await toast.present();
            return true;
        } else {

          const request = await this.api.post(`debts/${debt.uuid}/register`, []).subscribe(
            async response => {
              let message = "Não foi possível efetuar o registro do boleto. Aguarde uns instantes e tente novamente.";
              await this.clipboard.copy(debt.digitable_line);
              if (response.data.registered) {
                message = 'O boleto foi enviado para registro na instituição financeira, ';
                message += 'aguarde  alguns minutos.';
              }
              const toast = await this.toast.create({
                message: message,
                duration: 3000,
              });
              await toast.present();
            }
          );
        }
    }
}
