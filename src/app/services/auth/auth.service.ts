import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    public url = '';
    public userSubject = new Subject<any>();
    isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
    constructor(private apiService: ApiService) {
    }

    async getTokensByCode(provider, code) {
        const data = {
            provider: provider.uuid,
            code
        };

        const token = await this.apiService.post('auth/access-token-by-code', data).toPromise().
           catch((error) => ({error: true, message: error.error.message}));

        if (token.error) {
            return {error: true, message: token.message};
        }
        localStorage.setItem('ecidade_app_tokens', JSON.stringify(token.data));
        const user = {name: token.data.name, email: token.data.email};

        localStorage.setItem('user_data', JSON.stringify(user));
        this.userSubject.next({user});
        return token;
    }

    isLoggedIn()
    {
        const token = this.getAccessToken();
        const isLogged = token !== false;
        this.isAuthenticated.next(isLogged);
        return isLogged;
    }

    notifyUserData(user = null)
    {
        if (user != null) {
            this.userSubject.next({user});
            return;
        }
        if (this.isLoggedIn()) {
            const data = JSON.parse(localStorage.getItem('ecidade_app_tokens'));
            const userData = {name: data.name, email: data.email};
            this.userSubject.next({userData});
        }
    }
    getAccessToken() {
        const data = (localStorage.getItem('ecidade_app_tokens'));
        if (data === null) {
            return false;
        }

        return JSON.parse(data).access_token || false;
    }



}
