import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    public url = '';
    constructor(private httpClient: HttpClient) {
        this.url = environment.apiServer.urlApi;
    }

    /**
     * Realiza um post para o endereco da api
     *
     * @param endpoint
     * @param data
     * @param formData
     */
    public post(endpoint: string, data: any, formData = false): Observable<any> {

        // eslint-disable-next-line @typescript-eslint/naming-convention
        const headerRequest = {'Content-Type': 'application/json; charset=utf-8'};
        if (formData) {
            headerRequest['Content-Type'] = 'x';
        }
        return this.httpClient.post(`${this.url}/${endpoint}`, data, {headers: headerRequest});
    }

    /**
     * Realiza um get para o endereco da api
     *
     * @param endpoint
     */
    public get(endpoint: string, options = null): Observable<any> {
        return this.httpClient.get(`${this.url}/${endpoint}`);

    }

    public put(endpoint: string, data): Observable<any> {
        return this.httpClient.put(`${this.url}/${endpoint}`, data);
    }

    public delete(endpoint: string): Observable<any> {
        return this.httpClient.delete(`${this.url}/${endpoint}`);
    }

}
