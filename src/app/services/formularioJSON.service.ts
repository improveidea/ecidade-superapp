import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tipo } from '../models/tipo';
import { Formulario } from '../models/formulario';
import {ApiService} from './api/api.service';

@Injectable({
  providedIn: 'root'
})
export class FormularioJSONService {

  constructor(
    private api: ApiService
  ) { }

  getBody(tipo): Observable<Formulario> {
    return this.api.get(`forms/${tipo.id}/body`);
  }
}
