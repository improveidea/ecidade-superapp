import { Unlock, Block, Monetary, ValueChanged } from './../models/atributo';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AtributoService {

  private block = new Subject();
  private unlock = new Subject();
  private monetary = new Subject();
  private dialogClear = new Subject();
  private showLoading = new Subject();
  private fieldValueChanged = new Subject();

  constructor() { }

  sendEventBlock(data: Block) {
    this.block.next(data);
  }

  receiveEventBlock(): Observable<any> {
    return this.block.asObservable();
  }

  sendEventUnlock(data: Unlock) {
    this.unlock.next(data);
  }

  receiveEventUnlock(): Observable<any> {
    return this.unlock.asObservable();
  }

  sendMonetaryValue(data: Monetary) {
    this.monetary.next(data);
  }

  receiveMonetaryValue(): Observable<any> {
    return this.monetary.asObservable();
  }

  sendDialogClear() {
    this.dialogClear.next();
  }

  receiveDialogClear(): Observable<any> {
    return this.dialogClear.asObservable();
  }

  sendShowLoading(show: boolean) {
    this.showLoading.next(show);
  }

  receiveShowLoading(): Observable<any> {
    return this.showLoading.asObservable();
  }

  sendFieldValueChanged(data: ValueChanged) {
    this.fieldValueChanged.next(data);
  }

  receiveFieldValueChanged(): Observable<any> {
    return this.fieldValueChanged.asObservable();
  }
}
