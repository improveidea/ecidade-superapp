import {DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {LoaderInterceptor} from './services/http/interceptors/load.interceptor';
import {HttpConfigInterceptor} from './services/http/interceptors/bearer.interceptor';
import { File } from '@awesome-cordova-plugins/file/ngx';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';


import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import localePt from "@angular/common/locales/pt";

import {Clipboard} from "@awesome-cordova-plugins/clipboard/ngx";
import {registerLocaleData} from "@angular/common";

registerLocaleData(localePt, 'pt');

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot({mode: 'md'}),
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule
    ],
    providers: [
        InAppBrowser,
        FCM,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
        File,
        AndroidPermissions,
        FileOpener,
        Clipboard,
        {
            provide: LOCALE_ID,
            useValue: 'pt'
        },

        /* if you don't provide the currency symbol in the pipe,
        this is going to be the default symbol (R$) ... */
        {
            provide:  DEFAULT_CURRENCY_CODE,
            useValue: 'BRL'
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
