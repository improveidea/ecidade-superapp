import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function cnpjInvalidoValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {

    if (control == null || control.value == null || control.value === '') {
      return;
    }

    const cnpjDigitLength = 2;
    const cnpjLength = 14;
    const cnpj = control.value.replace(/\D/g, '');

    let digito;
    let digitos = '';
    let numerosCnpj = [];
    const cnpjInvalidoRetorno = {
      cnpjInvalido: {
        value: control.value,
        reason: '',
        digit: null
    }};

    // Verifica o tamanho da string
    if (cnpj.length !== cnpjLength) {
      cnpjInvalidoRetorno.cnpjInvalido.reason = 'different sizes';
      return cnpjInvalidoRetorno;
    }

    // Valida se todos os numeros sao iguais
    if (/^([0-9])\1*$/.test(cnpj)) {
      cnpjInvalidoRetorno.cnpjInvalido.reason = 'same number';
      return cnpjInvalidoRetorno;
    }

    let fator = 5;

    // Calcula o digito para verificar posteriormente
    for (let ind = cnpjDigitLength; ind >= 1; ind--) {

      if (ind === 1) {
        fator = 6;
      }

      numerosCnpj = cnpj.split('').reverse().slice(ind);
      digito      = numerosCnpj.reverse().reduce( (resultado, numero, i) => {

        if ( (i === 12 && ind === 2) || (i === 13 && ind === 1)) {
          return resultado;
        }

        resultado = resultado + numero * fator;
        fator--;

        if (fator === 1) {
          fator = 9;
        }

        return resultado;

      }, 0);

      digito   =  (11 - (digito % 11)) < 10 ? (11 - digito % 11) : 0;
      digitos += digito;
    }

    let cnpjCalculado  = cnpj.split('').reverse().slice(cnpjDigitLength).reverse().join('');
    cnpjCalculado += digitos;

    if (cnpjCalculado !== cnpj) {
      cnpjInvalidoRetorno.cnpjInvalido.reason = 'invalid digit';
      cnpjInvalidoRetorno.cnpjInvalido.digit  = digitos;
      return cnpjInvalidoRetorno;
    }

    return null;
  };
}


@Directive({
  selector: '[appCnpjInvalido]',
  providers: [{provide: NG_VALIDATORS, useExisting: CnpjInvalidoDirective, multi: true}]
})
export class CnpjInvalidoDirective {

  @Input('appCnpjInvalido') cnpjInvalido: string;

  validate(control: AbstractControl): {[key: string]: any} | null {
    return this.cnpjInvalido ? cnpjInvalidoValidator()(control) : null;
  }
}
