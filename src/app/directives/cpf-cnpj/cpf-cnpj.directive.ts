import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appCpfCnpj]'
})
export class CpfCnpjDirective {

  @Input() tipo: string;

  constructor(
    private el: ElementRef
  ) {
  }

  @HostListener('keyup', ['$event']) onkeyup(event: KeyboardEvent) {
    if (this.tipo.trim() !== 'cpfCnpj') {
      return false;
    }

    let cpfCnpj = this.el.nativeElement.value.replace(/\D/g, '').toString();

    if (cpfCnpj.length <= 11) {
      cpfCnpj = this.mascaraCPF(cpfCnpj);
    } else {
      cpfCnpj = this.mascaraCNPJ(cpfCnpj);
    }

    this.el.nativeElement.value = cpfCnpj;
  }

  private mascaraCPF(cpf) {
    cpf = cpf.replace(/(\d{1})(\d{8})$/, '$1.$2'); // coloca ponto antes dos ultimos 8 digitos
    cpf = cpf.replace(/(\d{1})(\d{5})$/, '$1.$2'); // coloca ponto antes dos ultimos 5 digitos
    cpf = cpf.replace(/(\d{1})(\d{2})$/, '$1-$2'); // coloca traÃ§o antes dos ultimos 2 digitos

    return cpf;
  }

  private mascaraCNPJ(cnpj: string) {
    cnpj = cnpj.replace(/(\d{1})(\d{12})$/, '$1.$2'); // coloca ponto antes dos ultimos 12 digitos
    cnpj = cnpj.replace(/(\d{1})(\d{9})$/, '$1.$2'); // coloca ponto antes dos ultimos 9 digitos
    cnpj = cnpj.replace(/(\d{1})(\d{6})$/, '$1/$2'); // coloca barra antes dos ultimos 6 digitos
    cnpj = cnpj.replace(/(\d{1})(\d{2})$/, '$1.$2'); // coloca ponto antes dos ultimos 2 digitos

    return cnpj;
  }
}
