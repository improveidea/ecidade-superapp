import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function cpfInvalidoValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {

    if (control === null || control.value === null || control.value === '') {
      return;
    }

    const cpfDigitLength = 2;
    const cpfLength = 11;
    const cpf = control.value.replace(/\D/g, '');
    let digito;
    let digitos = '';
    let numerosCpf = [];
    const cpfInvalidoRetorno = {
      cpfInvalido: {
        value: control.value,
        reason: ''
    }};

    // Verifica o tamanho da string
    if (cpf.length !== cpfLength) {

      cpfInvalidoRetorno.cpfInvalido.reason = 'different sizes';
      return cpfInvalidoRetorno;
    }

    // Valida se todos os numeros sao iguais
    if (/^([0-9])\1*$/.test(cpf)) {
      cpfInvalidoRetorno.cpfInvalido.reason = 'same number';
      return cpfInvalidoRetorno;
    }

    // Calcula o digito para verificar posteriormente
    for (let ind = cpfDigitLength; ind >= 1; ind--) {

      numerosCpf = cpf.split('').reverse().slice(ind);
      digito     = numerosCpf
                        .map((numero, i) => (numero * (i + cpfDigitLength)))
                        .reduce((total, atual) => (total + atual));

      digito   = digito % cpfLength;
      digito   = (digito < cpfDigitLength) ? 0 : cpfLength - digito;
      digitos += digito;
    }

    let cpfCalculado  = cpf.split('').reverse().slice(cpfDigitLength).reverse().join('');
    cpfCalculado += digitos;

    if (cpfCalculado !== cpf) {
      cpfInvalidoRetorno.cpfInvalido.reason = 'invalid digit';
      return cpfInvalidoRetorno;
    }

    return null;
  };
}

@Directive({
  selector: '[appCpfInvalido]',
  providers: [{provide: NG_VALIDATORS, useExisting: CpfInvalidoDirective, multi: true}]
})
export class CpfInvalidoDirective implements Validator {

  @Input('appCpfInvalido') cpfInvalido: string;

  validate(control: AbstractControl): {[key: string]: any} | null {
    return this.cpfInvalido ? cpfInvalidoValidator()(control) : null;
  }
}
