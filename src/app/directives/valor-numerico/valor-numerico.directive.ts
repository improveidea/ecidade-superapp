import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { AtributoService } from 'src/app/services/atributo.service';

@Directive({
  selector: '[valorMonetario]'
})
export class ValorNumericoDirective {

  @Input() tipo: string;
  @Input() campo: string = null;
  @Input() casasDecimais = '2';
  @Output() directiveChangedValue: EventEmitter<any> = new EventEmitter();

  constructor(
    private el: ElementRef,
    private readonly atributoService: AtributoService
  ) {}

  @HostListener('keyup', ['$event']) onkeyup(event: KeyboardEvent) {
    if (this.tipo.trim() === 'inteiro' || this.tipo.trim() === 'monetario' || this.tipo.trim() === 'numerico') {
      const valor = this.el.nativeElement.value.replace(/\D/g, '').toString();
      let valorFormatado = valor;

      if (this.tipo.trim() === 'monetario') {
        valorFormatado = 0;

        valorFormatado = valor; // permite digitar apenas numero
        valorFormatado = valorFormatado.toString().replace(/(\d{1})(\d{14})$/, '$1.$2'); // coloca ponto antes dos ultimos 14 digitos
        valorFormatado = valorFormatado.toString().replace(/(\d{1})(\d{11})$/, '$1.$2'); // coloca ponto antes dos ultimos 11 digitos
        valorFormatado = valorFormatado.toString().replace(/(\d{1})(\d{8})$/, '$1.$2'); // coloca ponto antes dos ultimos 8 digitos
        valorFormatado = valorFormatado.toString().replace(/(\d{1})(\d{5})$/, '$1.$2'); // coloca ponto antes dos ultimos 5 digitos
        valorFormatado = valorFormatado.toString().replace(/(\d{1})(\d{1,2})$/, '$1,$2'); // coloca virgula antes dos ultimos 2 digitos

        if (this.campo) {
          this.atributoService.sendMonetaryValue({
            field: this.campo.trim(),
            value: valorFormatado
          });
        }
      }

      if (this.tipo.trim() === 'numerico') {
        const regex = '(\\d{1})(\\d{1,' + this.casasDecimais + '})$';
        valorFormatado = valorFormatado.toString().replace(RegExp(regex), '$1.$2'); // coloca virgula antes dos ultimos 2 digitos
      }

      this.el.nativeElement.value = valorFormatado;
      this.changeValueEvent(valorFormatado);
    }
  }

  changeValueEvent(val) {
    this.directiveChangedValue.emit(val);
  }
}
