import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[onlyNumber]'
})
export class OnlyNumberDirective {

  constructor(private el: ElementRef) {}

  @HostListener('keyup', ['$event']) onkeyup(event: KeyboardEvent) {
      this.el.nativeElement.value = this.el.nativeElement.value.replace(/\D/g, '');
  }
}
