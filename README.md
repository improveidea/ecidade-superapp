#E-Cidade SuperApp

## - Instalação

Aplicativo desenvolvido com Ionic Framwork 5.

####Para executar a instalação deste projeto, é necessário já ter instalado o projeto `API-SuperApp`

Para instalar o projeto, execute `npm install`.

## - Desenvolvimento
Para iniciar o modo desenvolvimento execute os seguintes passos:
1. crie ou altere o arquivo `src/environments/environment.ts`.
2. altere o objeto `apiServer: {
   urlApi: 'http://192.168.20.66:8090/api',
   secret: '',
   client: ''
   }` que se referem ao projeto `api-superapp`.
3. execute `ionic serve`

Feito isso pode ser acessado através de `http://localhost:8100`.

##- Build
Execute: `ionic cordova build [android|ios]`

#### \> Para Android
O processo de build através da linha de comando constuma dar problema, por isso após rodar 
o build abra o Android Studio e adicione o projeto localizado em `platforms/android`.

Após acesse o menu `Build > Build Bundle(s) / APK > Build APK(s)`.

#### \> Para Ios/Iphone
Para criar o o build para IOS, o processo deve ser feito um mac (macOS), 

Para isso, precisamos de o Xcode instalado no computador.
O Primeiro build, devemos rodar o comando `ionic cordova build ios`. Após o build, devemos abrir
e xCode na pasta  `platforms/ios` e clicar em build. Podemos criar o build diretamente em um Iphone, 
ou usar um Emulador disponibilizado no xCode.

